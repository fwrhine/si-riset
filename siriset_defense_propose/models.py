from django.db import models

# Create your models here.
class Defense(models.Model):
    defense_id = models.IntegerField(primary_key=True)
    research = models.ForeignKey('siriset_research_apply.Research', models.DO_NOTHING)
    student_npm = models.ForeignKey('siriset_login.Student', models.DO_NOTHING, db_column='student_npm')
    room = models.CharField(max_length=10)
    datetime = models.DateField()
    schedule_by = models.ForeignKey('siriset_login.AcademicAdministrator', models.DO_NOTHING, db_column='schedule_by')

    class Meta:
        managed = False
        db_table = 'defense'
