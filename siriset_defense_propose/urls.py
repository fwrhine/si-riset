from django.conf.urls import url
from .views import index, propose
#url for app

app_name = 'defense'

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^propose/$', propose, name='propose'),
]
