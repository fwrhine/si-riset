from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.db import connection

response = {}

def index(request):
    response['author'] = "SIRISET"
    cursor = connection.cursor()
    cursor.execute('SELECT r.research_id, rt.research_type_name, r.topic FROM research as r, research_type as rt WHERE r.research_type_id = rt.research_type_id AND r.student_npm = %s', [request.session['identity_code']])
    research = fetchasdict(cursor)
    response['research'] = research
    return render(request, 'defense_propose.html', response)

def fetchasdict(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

@csrf_exempt
def propose(request):
    if request.method == 'POST':
        research_id = request.POST['id']
    cursor = connection.cursor()
    cursor.execute("INSERT INTO defense (defense_id, research_id, student_npm) VALUES (nextval('defenseid'), %s,  %s)", [research_id, request.session['identity_code']])
    data = {'id': research_id}
    return JsonResponse(data)
