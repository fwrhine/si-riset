from django.apps import AppConfig


class SirisetDefenseProposeConfig(AppConfig):
    name = 'siriset_defense_propose'
