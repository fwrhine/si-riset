# Generated by Django 2.0.5 on 2018-05-26 02:05

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Defense',
            fields=[
                ('defense_id', models.IntegerField(primary_key=True, serialize=False)),
                ('room', models.CharField(max_length=10)),
                ('datetime', models.DateField()),
            ],
            options={
                'db_table': 'defense',
                'managed': False,
            },
        ),
    ]
