from django.conf.urls import url
from .views import index, logbook, approve, add, add_form
#url for app

app_name = 'log'

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^view/$', logbook, name='view'),
    url(r'^add/$', add, name='add'),
    url(r'^add-form/$', add_form, name='add-form'),
    url(r'^approve/$', approve, name='approve'),
]
