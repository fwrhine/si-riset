from django.db import models

# Create your models here.
class Logbook(models.Model):
    logbook_id = models.IntegerField(primary_key=True)
    datetime = models.DateField()
    location = models.CharField(max_length=50)
    summary = models.CharField(max_length=500)
    plan = models.CharField(max_length=500)
    research = models.ForeignKey('siriset_research_apply.Research', models.DO_NOTHING)
    supervisor = models.ForeignKey('siriset_login.Supervisor', models.DO_NOTHING)
    status = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'logbook'
