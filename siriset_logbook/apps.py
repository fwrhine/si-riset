from django.apps import AppConfig


class SirisetLogbookConfig(AppConfig):
    name = 'siriset_logbook'
