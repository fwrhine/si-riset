from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.db import connection, transaction
from django.http import JsonResponse
from django.urls import reverse

response = {}

@csrf_exempt
def index(request):
    cursor = connection.cursor()

    if request.session['role'] == "student":
        sql = "SELECT rt.research_type_name, r.topic, r.research_id\
            FROM research r, research_type rt\
            WHERE r.research_type_id = rt.research_type_id and r.research_id = %s"
        cursor.execute(sql, [request.session["research_id"]])

    elif request.session['role'] == "supervisor":
        sql = "SELECT u.name, sp.degree, rt.research_type_name, r.topic, r.research_id\
            FROM research r, research_type rt, user_ u, student s, study_program sp\
            WHERE r.research_type_id = rt.research_type_id and r.student_npm = s.npm\
            and s.sso_username = u.sso_username and s.program_id = sp.program_id\
            and r.research_id = %s"
        cursor.execute(sql, [request.session["research_id"]])

    research = dictfetchall(cursor)[0]

    sql = "SELECT l.logbook_id, l.datetime, l.location, l.summary, l.plan, l.status\
        FROM logbook l WHERE l.research_id = %s"

    cursor.execute(sql, [request.session["research_id"]])
    logbook = dictfetchall(cursor)

    response['author'] = "si-riset"
    response['research'] = research
    response['logbook'] = logbook

    return render(request, 'logbook_view.html', response)

@csrf_exempt
def logbook(request):
    print("masuk logbook")
    if request.method == 'POST':
        request.session["research_id"] = request.POST['id']

    return JsonResponse({'redirect_url': '/logbook/'})

@csrf_exempt
def approve(request):
    if request.method == 'POST':
        logbook_id = request.POST['id']

    cursor = connection.cursor()

    sql = "UPDATE logbook SET status = 'approved' WHERE logbook_id = %s"
    cursor.execute(sql, [logbook_id])

    return JsonResponse({'redirect_url': '/logbook/'})

def add_form(request):

    return render(request, 'logbook_add.html', response)

@csrf_exempt
def add(request):
    print("keluar ga")
    if request.method == 'POST':
        print(request.POST)
        date = request.POST['date']
        location = request.POST['loc']
        summary = request.POST['summary']
        plan = request.POST['plan']

    research_id = request.session["research_id"]

    cursor = connection.cursor()

    sql = "SELECT supervisor_id FROM research WHERE research_id = %s"

    cursor.execute(sql, [research_id])
    supervisor_id = dictfetchall(cursor)[0]['supervisor_id']

    sql = "INSERT INTO logbook (logbook_id, datetime, location, summary, plan, research_id,\
        supervisor_id, status) VALUES (nextval('LogbookId'), %s, %s, %s, %s, %s, %s, 'not approved')"

    cursor.execute(sql, [date, location, summary, plan, research_id, supervisor_id])

    return JsonResponse({'redirect_url': '/logbook/'})

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
