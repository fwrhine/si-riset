from django.db import models

# Create your models here.
class ApprovedResearch(models.Model):
    approval_id = models.IntegerField(primary_key=True)
    research = models.ForeignKey('siriset_research_apply.Research', models.DO_NOTHING)
    student_npm = models.ForeignKey('siriset_login.Student', models.DO_NOTHING, db_column='student_npm')
    supervisor = models.ForeignKey('siriset_login.Supervisor', models.DO_NOTHING)
    head_program = models.ForeignKey('siriset_login.HeadStudyProgram', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'approved_research'
