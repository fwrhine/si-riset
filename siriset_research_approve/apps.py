from django.apps import AppConfig


class SirisetResearchApproveConfig(AppConfig):
    name = 'siriset_research_approve'
