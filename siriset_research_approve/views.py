from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.db import connection, transaction

response = {}

def index(request):
    cursor = connection.cursor()
    if request.session['role'] == "supervisor":
        sql = 'SELECT r.research_id, r.student_npm, rt.research_type_name, r.topic, r.motivation\
            FROM research r, research_type rt, supervisor s\
            WHERE r.research_type_id = rt.research_type_id and r.supervisor_id = s.supervisor_id\
            and s.sso_username = %s and not exists (SELECT 1 FROM approved_research a\
            WHERE a.research_id = r.research_id)'
        cursor.execute(sql, [request.session['user_login']])
    elif request.session['role'] == "head_study_program":
        sql = 'SELECT a.research_id, a.student_npm, u.name, rt.research_type_name, r.topic, r.motivation\
            FROM approved_research a, research r, research_type rt, supervisor s, user_ u\
            WHERE a.research_id = r.research_id and r.research_type_id = rt.research_type_id\
            and a.supervisor_id = s.supervisor_id and s.sso_username = u.sso_username and a.head_program_id is null'
        cursor.execute(sql)

    research = dictfetchall(cursor)

    response['author'] = "si-riset"
    response['research'] = research
    return render(request, 'research_approve.html', response)

@csrf_exempt
def approve(request):
    if request.method == 'POST':
        research_id = request.POST['id']

    cursor = connection.cursor()

    if request.session['role'] == "supervisor" :
        sql = 'SELECT r.student_npm, r.supervisor_id FROM research r\
            WHERE r.research_id = %s'

        cursor.execute(sql, [research_id])
        research = dictfetchall(cursor)

        sql = "INSERT INTO approved_research (approval_id, research_id, student_npm,\
            supervisor_id, head_program_id) VALUES (nextval('ApprovalId'), %s, %s, %s, null)"

        cursor.execute(sql, [research_id, research[0]['student_npm'],\
            research[0]['supervisor_id']])

    elif request.session['role'] == "head_study_program":
        sql = 'UPDATE approved_research SET head_program_id = 1 WHERE research_id = %s'

        cursor.execute(sql, [research_id])

    data = {'id': research_id}
    return JsonResponse(data)

@csrf_exempt
def reject(request):
    if request.method == 'POST':
        research_id = request.POST['id']

    cursor = connection.cursor()

    if request.session['role'] == "supervisor" :
        sql = 'DELETE FROM research WHERE research_id = %s'

        cursor.execute(sql, [research_id])

    elif request.session['role'] == "head_study_program":
        sql = 'DELETE FROM approved_research WHERE research_id = %s'
        cursor.execute(sql, [research_id])
        sql = 'DELETE FROM research WHERE research_id = %s'
        cursor.execute(sql, [research_id])

    data = {'id': research_id}
    return JsonResponse(data)

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
