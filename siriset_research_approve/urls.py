from django.conf.urls import url
from .views import index, approve, reject
#url for app

app_name = 'approver'

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^approve/$', approve, name='approve'),
    url(r'^reject/$', reject, name='reject'),
]
