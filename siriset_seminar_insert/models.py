from django.db import models

# Create your models here.
class SeminarReboan(models.Model):
    seminar_id = models.IntegerField(primary_key=True)
    topic = models.CharField(max_length=50)
    speaker = models.CharField(max_length=50)
    arranged_by = models.ForeignKey('siriset_login.ResearchManager', models.DO_NOTHING, db_column='arranged_by')

    class Meta:
        managed = False
        db_table = 'seminar_reboan'
