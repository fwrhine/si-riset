from django.apps import AppConfig


class SirisetSeminarInsertConfig(AppConfig):
    name = 'siriset_seminar_insert'
