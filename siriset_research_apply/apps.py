from django.apps import AppConfig


class SirisetResearchApplyConfig(AppConfig):
    name = 'siriset_research_apply'
