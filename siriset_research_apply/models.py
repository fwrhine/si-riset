from django.db import models
# from siriset_login.models import Student
# from siriset_login.models import Supervisor

# Create your models here.
class ResearchType(models.Model):
    research_type_id = models.IntegerField(primary_key=True)
    research_type_name = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'research_type'

class Research(models.Model):
    research_id = models.IntegerField(primary_key=True)
    research_type = models.ForeignKey('ResearchType', models.DO_NOTHING)
    topic = models.CharField(max_length=50)
    motivation = models.CharField(max_length=500)
    student_npm = models.ForeignKey('siriset_login.Student', models.DO_NOTHING, db_column='student_npm')
    supervisor = models.ForeignKey('siriset_login.Supervisor', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'research'
