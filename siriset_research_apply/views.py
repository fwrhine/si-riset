from django.shortcuts import render
from django.db import connection, transaction
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse

response = {}
@csrf_exempt
def index(request):
    cursor = connection.cursor()

    sql = 'SELECT rt.research_type_name, rt.research_type_id\
        FROM research_type rt'

    cursor.execute(sql)
    research = dictfetchall(cursor)

    sql = 'SELECT s.supervisor_id, u.name\
        FROM supervisor s, user_ u\
        WHERE s.sso_username = u.sso_username'

    cursor.execute(sql)
    supervisor = dictfetchall(cursor)

    response['author'] = "si-riset"
    response['research'] = research
    response['supervisor'] = supervisor
    return render(request, 'research_apply.html', response)


@csrf_exempt
def apply_research(request):
    if(request.method == 'POST'):
        print(request.POST)
        research_type = request.POST['rt']
        topic = request.POST['topic']
        motivation = request.POST['motivation']
        supervisor = request.POST['supervisor']

        sql1 = "SELECT s.npm FROM student s, user_ u WHERE s.sso_username = u.sso_username\
            and u.sso_username = %s"

        sql2 =  "INSERT INTO research (research_id, research_type_id, topic, motivation, student_npm, supervisor_id)\
            VALUES (nextval('ResearchId'), %s, %s, %s, %s, %s)"

        cursor = connection.cursor()

        cursor.execute(sql1, [request.session['user_login']])
        npm = dictfetchall(cursor)[0]['npm']

        cursor.execute(sql2, [research_type, topic, motivation, npm, supervisor])

        return JsonResponse({'redirect_url': '/apply-research/'})

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
