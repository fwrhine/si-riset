from django.conf.urls import url
from .views import index, apply_research
#url for app

app_name = 'applyr'

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^apply/', apply_research, name='apply'),
]
