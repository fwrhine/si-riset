# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AcademicAdministrator(models.Model):
    admin_id = models.IntegerField(primary_key=True)
    sso_username = models.ForeignKey('User', models.DO_NOTHING, db_column='sso_username')

    class Meta:
        managed = False
        db_table = 'academic_administrator'


class ApprovedResearch(models.Model):
    approval_id = models.IntegerField(primary_key=True)
    research = models.ForeignKey('Research', models.DO_NOTHING)
    student_npm = models.ForeignKey('Student', models.DO_NOTHING, db_column='student_npm')
    supervisor = models.ForeignKey('Supervisor', models.DO_NOTHING)
    head_program = models.ForeignKey('HeadStudyProgram', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'approved_research'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class Defense(models.Model):
    defense_id = models.IntegerField(primary_key=True)
    research = models.ForeignKey('Research', models.DO_NOTHING)
    student_npm = models.ForeignKey('Student', models.DO_NOTHING, db_column='student_npm')
    room = models.CharField(max_length=10)
    datetime = models.DateField()
    schedule_by = models.ForeignKey(AcademicAdministrator, models.DO_NOTHING, db_column='schedule_by')

    class Meta:
        managed = False
        db_table = 'defense'


class DefenseExaminer(models.Model):
    defense = models.ForeignKey(Defense, models.DO_NOTHING, primary_key=True)
    examiner = models.ForeignKey('FacultyMember', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'defense_examiner'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class FacultyMember(models.Model):
    faculty_member_id = models.IntegerField(primary_key=True)
    education_level = models.CharField(max_length=20)
    sso_username = models.ForeignKey('User', models.DO_NOTHING, db_column='sso_username')

    class Meta:
        managed = False
        db_table = 'faculty_member'


class HeadStudyProgram(models.Model):
    head_id = models.IntegerField(primary_key=True)
    program = models.ForeignKey('StudyProgram', models.DO_NOTHING)
    faculty_member = models.ForeignKey(FacultyMember, models.DO_NOTHING)
    sso_username = models.ForeignKey('User', models.DO_NOTHING, db_column='sso_username')
    status = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'head_study_program'


class Logbook(models.Model):
    logbook_id = models.IntegerField(primary_key=True)
    datetime = models.DateField()
    location = models.CharField(max_length=50)
    summary = models.CharField(max_length=500)
    plan = models.CharField(max_length=500)
    research = models.ForeignKey('Research', models.DO_NOTHING)
    supervisor = models.ForeignKey('Supervisor', models.DO_NOTHING)
    status = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'logbook'


class Research(models.Model):
    research_id = models.IntegerField(primary_key=True)
    research_type = models.ForeignKey('ResearchType', models.DO_NOTHING)
    topic = models.CharField(max_length=50)
    motivation = models.CharField(max_length=500)
    student_npm = models.ForeignKey('Student', models.DO_NOTHING, db_column='student_npm')
    supervisor = models.ForeignKey('Supervisor', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'research'


class ResearchManager(models.Model):
    research_manager_id = models.IntegerField(primary_key=True)
    sso_username = models.ForeignKey('User', models.DO_NOTHING, db_column='sso_username')
    status = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'research_manager'


class ResearchType(models.Model):
    research_type_id = models.IntegerField(primary_key=True)
    research_type_name = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'research_type'


class ScientificPaper(models.Model):
    paper_id = models.IntegerField(primary_key=True)
    paper_title = models.CharField(max_length=50)
    paper_type = models.CharField(max_length=20)
    student_npm = models.ForeignKey('Student', models.DO_NOTHING, db_column='student_npm')
    submission_date = models.DateField()

    class Meta:
        managed = False
        db_table = 'scientific_paper'


class SeminarReboan(models.Model):
    seminar_id = models.IntegerField(primary_key=True)
    topic = models.CharField(max_length=50)
    speaker = models.CharField(max_length=50)
    arranged_by = models.ForeignKey(ResearchManager, models.DO_NOTHING, db_column='arranged_by')

    class Meta:
        managed = False
        db_table = 'seminar_reboan'


class Student(models.Model):
    npm = models.CharField(primary_key=True, max_length=20)
    program = models.ForeignKey('StudyProgram', models.DO_NOTHING)
    sso_username = models.ForeignKey('User', models.DO_NOTHING, db_column='sso_username')

    class Meta:
        managed = False
        db_table = 'student'


class StudyProgram(models.Model):
    program_id = models.IntegerField(primary_key=True)
    degree = models.CharField(max_length=20)
    program_name = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'study_program'


class Supervisor(models.Model):
    supervisor_id = models.IntegerField(primary_key=True)
    sso_username = models.ForeignKey('User', models.DO_NOTHING, db_column='sso_username')

    class Meta:
        managed = False
        db_table = 'supervisor'


class User(models.Model):
    sso_username = models.CharField(primary_key=True, max_length=20)
    sso_password = models.CharField(max_length=20)
    name = models.CharField(max_length=50)
    email = models.CharField(max_length=50)
    phone_no = models.CharField(max_length=15)

    class Meta:
        managed = False
        db_table = 'user_'
