"""settings URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.views.generic.base import RedirectView

import siriset_login.urls as login
import siriset_profile.urls as profile
import siriset_research_apply.urls as applyr
import siriset_research_approve.urls as approver
import siriset_research_view_approved.urls as viewappr
import siriset_logbook.urls as log
import siriset_defense_propose.urls as defense
import siriset_defense_schedule.urls as dsched
import siriset_scipaper_submit.urls as scipaper
import siriset_seminar_insert.urls as seminar

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', include((login, login), namespace='login')),
    path('profile/', include((profile, profile), namespace='profile')),
    path('apply-research/', include((applyr, applyr), namespace='apply-research')),
    path('approve-research/', include((approver, approver), namespace='approve-research')),
    path('view-approved/', include((viewappr, viewappr), namespace='view-approved')),
    path('logbook/', include((log, log), namespace='logbook')),
    path('propose-defense/', include((defense, defense), namespace='defense')),
    path('defense-schedule/', include((dsched, dsched), namespace='defense-schedule')),
    path('submit-paper/', include((scipaper, scipaper), namespace='scipaper')),
    path('insert-seminar/', include((seminar, seminar), namespace='seminar')),
    path('', RedirectView.as_view(url='login/student', permanent=True), name='index'),
]
