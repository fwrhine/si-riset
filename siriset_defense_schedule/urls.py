from django.conf.urls import url
from .views import index, schedule, form_schedule, apply_schedule
#url for app

app_name = 'dsched'

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^schedule/$', schedule, name='schedule'),
    url(r'^form-schedule/$', form_schedule, name='form-schedule'),
    url(r'^apply-schedule/$', apply_schedule, name='apply-schedule'),
]
