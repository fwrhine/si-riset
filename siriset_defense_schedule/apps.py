from django.apps import AppConfig


class SirisetDefenseScheduleConfig(AppConfig):
    name = 'siriset_defense_schedule'
