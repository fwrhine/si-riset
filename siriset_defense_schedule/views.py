from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.db import connection
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

response = {}

def index(request):
    response['author'] = "SIRISET"
    cursor = connection.cursor()
    param = 'SELECT defense.defense_id, research_type.research_type_name, defense.student_npm, user_.name\
            FROM ((((defense INNER JOIN research ON defense.research_id = research.research_id) INNER JOIN research_type ON research.research_type_id = research_type.research_type_id)\
            INNER JOIN supervisor ON research.supervisor_id = supervisor.supervisor_id) INNER JOIN user_ on supervisor.sso_username = user_.sso_username);'

    cursor.execute(param)
    defense = fetchasdict(cursor)
    response['defense'] = defense
    return render(request, 'defense_schedule.html', response)

def fetchasdict(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

@csrf_exempt
def schedule(request):
    if request.method == 'POST':
        defense_id = request.POST['id']

    cursor = connection.cursor()
    param = 'SELECT defense.defense_id, research_type.research_type_name, defense.student_npm, user_.name\
            FROM ((((defense INNER JOIN research ON research.research_id = defense.research_id AND defense.defense_id = %s) INNER JOIN research_type ON research.research_type_id = research_type.research_type_id)\
            INNER JOIN supervisor ON research.supervisor_id = supervisor.supervisor_id) INNER JOIN user_ on supervisor.sso_username = user_.sso_username);'
    cursor.execute(param, [defense_id])
    schedule = cursor.fetchone()
    request.session["defense_id"] = schedule[0]
    request.session["research_type_name"] = schedule[1]
    request.session["student_npm"] = schedule[2]
    request.session["user_name"] = schedule[3]
    return JsonResponse({'redirect_url': '/defense-schedule/form-schedule'})

def form_schedule(request):
    cursor = connection.cursor()
    param = 'SELECT user_.name, faculty_member.faculty_member_id\
            FROM (faculty_member INNER JOIN user_ ON faculty_member.sso_username = user_.sso_username AND faculty_member.faculty_member_id != 1);'

    cursor.execute(param)
    examiner = fetchasdict(cursor)
    response['examiner'] = examiner
    response['defense_id'] = request.session["defense_id"]
    response['research_type_name'] = request.session["research_type_name"]
    response['student_npm'] = request.session["student_npm"]
    response['user_name'] = request.session["user_name"]
    html = "form_schedule.html"
    return render(request, html, response)

def apply_schedule(request):
    if request.method == 'POST':
        date_ = request.POST['date_']
        room = request.POST['room']
        examiner = request.POST.get('examiner',False)

    cursor = connection.cursor()

    # UPDATE DATE, ROOM and TIME
    param = 'UPDATE defense SET(room,datetime,schedule_by) = (%s, %s, %s) WHERE defense_id = %s'
    cursor.execute(param, [room, date_, request.session['identity_code'], request.session["defense_id"]])

    # # GET EXAMINER ID
    # param = 'SELECT fm.faculty_member_id\
    #         FROM faculty_member as fm, user_ as u\
    #         WHERE fm.sso_username = u.sso_username AND u.name = %s;'
    # cursor.execute(param, [examiner])
    # examiner_id = cursor.fetchone()

    # Check if defense_id is exist
    print(request.session['defense_id'])
    param = 'SELECT defense_id FROM defense_examiner WHERE defense_id=%s;'
    cursor.execute(param, [request.session['defense_id']]);
    check = cursor.fetchone();
    if check is not None: # Update if exist
        param = 'UPDATE defense_examiner SET examiner_id = %s WHERE defense_id = %s;'
        cursor.execute(param, [ examiner, request.session['defense_id'] ]);
    else: # Add if not
        param = "INSERT INTO defense_examiner (defense_id, examiner_id) VALUES (%s, %s);"
        cursor.execute(param, [request.session['defense_id'], examiner])

    return HttpResponseRedirect(reverse('defense-schedule:index'))
