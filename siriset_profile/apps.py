from django.apps import AppConfig


class SirisetProfileConfig(AppConfig):
    name = 'siriset_profile'
