# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from siriset_login.views import authenticate
from django.db import connection

response = {}

def index(request):
    if not authenticate(request):
        return HttpResponseRedirect(reverse('login:index', kwargs={'role':request.session['role']}))
    else:
        response['is_login'] = True
        response['author'] = 'SIRISET'

        cursor = connection.cursor()
        cursor.execute('SELECT * FROM user_ WHERE sso_username = %s;', [request.session['user_login']])
        user = cursor.fetchone()
        response['username'] = user[0]
        response['name'] = user[2]
        response['email'] = user[3]
        response['phone'] = user[4]
        response['role'] = request.session['role']
        response['identity_code'] = request.session['identity_code']

        if request.session['role'] == 'student':
            response['program_id'] = request.session['program_id']

        elif request.session['role'] == 'research_manager':
            response['status'] = request.session['status']

        elif request.session['role'] == 'head_study_program':
            response['program_id'] = request.session['program_id']
            response['status'] = request.session['status']
            response['faculty_member_id'] = request.session['faculty_member_id']

        elif request.session['role'] == 'defense_examiner':
            response['faculty_member_id'] = request.session['faculty_member_id']

    html = 'profile.html'
    return render(request, html, response)
