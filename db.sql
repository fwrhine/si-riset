--
-- PostgreSQL database dump
--

-- Dumped from database version 10.3
-- Dumped by pg_dump version 10.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: si_riset; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA si_riset;


ALTER SCHEMA si_riset OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: academic_administrator; Type: TABLE; Schema: si_riset; Owner: postgres
--

CREATE TABLE si_riset.academic_administrator (
    admin_id integer NOT NULL,
    sso_username character varying(20) NOT NULL
);


ALTER TABLE si_riset.academic_administrator OWNER TO postgres;

--
-- Name: approved_research; Type: TABLE; Schema: si_riset; Owner: postgres
--

CREATE TABLE si_riset.approved_research (
    approval_id integer NOT NULL,
    research_id integer NOT NULL,
    student_npm character varying(20) NOT NULL,
    supervisor_id integer NOT NULL,
    head_program_id integer
);


ALTER TABLE si_riset.approved_research OWNER TO postgres;

--
-- Name: approvalid; Type: SEQUENCE; Schema: si_riset; Owner: postgres
--

CREATE SEQUENCE si_riset.approvalid
    START WITH 51
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2000
    CACHE 1;


ALTER TABLE si_riset.approvalid OWNER TO postgres;

--
-- Name: approvalid; Type: SEQUENCE OWNED BY; Schema: si_riset; Owner: postgres
--

ALTER SEQUENCE si_riset.approvalid OWNED BY si_riset.approved_research.approval_id;


--
-- Name: auth_group; Type: TABLE; Schema: si_riset; Owner: siriset
--

CREATE TABLE si_riset.auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE si_riset.auth_group OWNER TO siriset;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: si_riset; Owner: siriset
--

CREATE SEQUENCE si_riset.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE si_riset.auth_group_id_seq OWNER TO siriset;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: si_riset; Owner: siriset
--

ALTER SEQUENCE si_riset.auth_group_id_seq OWNED BY si_riset.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: si_riset; Owner: siriset
--

CREATE TABLE si_riset.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE si_riset.auth_group_permissions OWNER TO siriset;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: si_riset; Owner: siriset
--

CREATE SEQUENCE si_riset.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE si_riset.auth_group_permissions_id_seq OWNER TO siriset;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: si_riset; Owner: siriset
--

ALTER SEQUENCE si_riset.auth_group_permissions_id_seq OWNED BY si_riset.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: si_riset; Owner: siriset
--

CREATE TABLE si_riset.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE si_riset.auth_permission OWNER TO siriset;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: si_riset; Owner: siriset
--

CREATE SEQUENCE si_riset.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE si_riset.auth_permission_id_seq OWNER TO siriset;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: si_riset; Owner: siriset
--

ALTER SEQUENCE si_riset.auth_permission_id_seq OWNED BY si_riset.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: si_riset; Owner: siriset
--

CREATE TABLE si_riset.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE si_riset.auth_user OWNER TO siriset;

--
-- Name: auth_user_groups; Type: TABLE; Schema: si_riset; Owner: siriset
--

CREATE TABLE si_riset.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE si_riset.auth_user_groups OWNER TO siriset;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: si_riset; Owner: siriset
--

CREATE SEQUENCE si_riset.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE si_riset.auth_user_groups_id_seq OWNER TO siriset;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: si_riset; Owner: siriset
--

ALTER SEQUENCE si_riset.auth_user_groups_id_seq OWNED BY si_riset.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: si_riset; Owner: siriset
--

CREATE SEQUENCE si_riset.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE si_riset.auth_user_id_seq OWNER TO siriset;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: si_riset; Owner: siriset
--

ALTER SEQUENCE si_riset.auth_user_id_seq OWNED BY si_riset.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: si_riset; Owner: siriset
--

CREATE TABLE si_riset.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE si_riset.auth_user_user_permissions OWNER TO siriset;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: si_riset; Owner: siriset
--

CREATE SEQUENCE si_riset.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE si_riset.auth_user_user_permissions_id_seq OWNER TO siriset;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: si_riset; Owner: siriset
--

ALTER SEQUENCE si_riset.auth_user_user_permissions_id_seq OWNED BY si_riset.auth_user_user_permissions.id;


--
-- Name: defense; Type: TABLE; Schema: si_riset; Owner: postgres
--

CREATE TABLE si_riset.defense (
    defense_id integer NOT NULL,
    research_id integer NOT NULL,
    student_npm character varying(20) NOT NULL,
    room character varying(10),
    datetime date,
    schedule_by integer
);


ALTER TABLE si_riset.defense OWNER TO postgres;

--
-- Name: defense_examiner; Type: TABLE; Schema: si_riset; Owner: postgres
--

CREATE TABLE si_riset.defense_examiner (
    defense_id integer NOT NULL,
    examiner_id integer NOT NULL
);


ALTER TABLE si_riset.defense_examiner OWNER TO postgres;

--
-- Name: defenseid; Type: SEQUENCE; Schema: si_riset; Owner: postgres
--

CREATE SEQUENCE si_riset.defenseid
    START WITH 21
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 1000
    CACHE 1;


ALTER TABLE si_riset.defenseid OWNER TO postgres;

--
-- Name: defenseid; Type: SEQUENCE OWNED BY; Schema: si_riset; Owner: postgres
--

ALTER SEQUENCE si_riset.defenseid OWNED BY si_riset.defense.defense_id;


--
-- Name: django_admin_log; Type: TABLE; Schema: si_riset; Owner: siriset
--

CREATE TABLE si_riset.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE si_riset.django_admin_log OWNER TO siriset;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: si_riset; Owner: siriset
--

CREATE SEQUENCE si_riset.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE si_riset.django_admin_log_id_seq OWNER TO siriset;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: si_riset; Owner: siriset
--

ALTER SEQUENCE si_riset.django_admin_log_id_seq OWNED BY si_riset.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: si_riset; Owner: siriset
--

CREATE TABLE si_riset.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE si_riset.django_content_type OWNER TO siriset;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: si_riset; Owner: siriset
--

CREATE SEQUENCE si_riset.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE si_riset.django_content_type_id_seq OWNER TO siriset;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: si_riset; Owner: siriset
--

ALTER SEQUENCE si_riset.django_content_type_id_seq OWNED BY si_riset.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: si_riset; Owner: siriset
--

CREATE TABLE si_riset.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE si_riset.django_migrations OWNER TO siriset;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: si_riset; Owner: siriset
--

CREATE SEQUENCE si_riset.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE si_riset.django_migrations_id_seq OWNER TO siriset;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: si_riset; Owner: siriset
--

ALTER SEQUENCE si_riset.django_migrations_id_seq OWNED BY si_riset.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: si_riset; Owner: siriset
--

CREATE TABLE si_riset.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE si_riset.django_session OWNER TO siriset;

--
-- Name: faculty_member; Type: TABLE; Schema: si_riset; Owner: postgres
--

CREATE TABLE si_riset.faculty_member (
    faculty_member_id integer NOT NULL,
    education_level character varying(20) NOT NULL,
    sso_username character varying(20) NOT NULL
);


ALTER TABLE si_riset.faculty_member OWNER TO postgres;

--
-- Name: head_study_program; Type: TABLE; Schema: si_riset; Owner: postgres
--

CREATE TABLE si_riset.head_study_program (
    head_id integer NOT NULL,
    program_id integer NOT NULL,
    faculty_member_id integer NOT NULL,
    sso_username character varying(20) NOT NULL,
    status character varying(20) NOT NULL
);


ALTER TABLE si_riset.head_study_program OWNER TO postgres;

--
-- Name: logbook; Type: TABLE; Schema: si_riset; Owner: postgres
--

CREATE TABLE si_riset.logbook (
    logbook_id integer NOT NULL,
    datetime date NOT NULL,
    location character varying(50) NOT NULL,
    summary character varying(500) NOT NULL,
    plan character varying(500) NOT NULL,
    research_id integer NOT NULL,
    supervisor_id integer NOT NULL,
    status character varying(20) NOT NULL
);


ALTER TABLE si_riset.logbook OWNER TO postgres;

--
-- Name: logbookid; Type: SEQUENCE; Schema: si_riset; Owner: postgres
--

CREATE SEQUENCE si_riset.logbookid
    START WITH 51
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2000
    CACHE 1;


ALTER TABLE si_riset.logbookid OWNER TO postgres;

--
-- Name: logbookid; Type: SEQUENCE OWNED BY; Schema: si_riset; Owner: postgres
--

ALTER SEQUENCE si_riset.logbookid OWNED BY si_riset.logbook.logbook_id;


--
-- Name: research; Type: TABLE; Schema: si_riset; Owner: postgres
--

CREATE TABLE si_riset.research (
    research_id integer NOT NULL,
    research_type_id integer NOT NULL,
    topic character varying(50) NOT NULL,
    motivation character varying(500) NOT NULL,
    student_npm character varying(20) NOT NULL,
    supervisor_id integer NOT NULL
);


ALTER TABLE si_riset.research OWNER TO postgres;

--
-- Name: research_manager; Type: TABLE; Schema: si_riset; Owner: postgres
--

CREATE TABLE si_riset.research_manager (
    research_manager_id integer NOT NULL,
    sso_username character varying(20) NOT NULL,
    status character varying(20) NOT NULL
);


ALTER TABLE si_riset.research_manager OWNER TO postgres;

--
-- Name: research_type; Type: TABLE; Schema: si_riset; Owner: postgres
--

CREATE TABLE si_riset.research_type (
    research_type_id integer NOT NULL,
    research_type_name character varying(30) NOT NULL
);


ALTER TABLE si_riset.research_type OWNER TO postgres;

--
-- Name: researchid; Type: SEQUENCE; Schema: si_riset; Owner: postgres
--

CREATE SEQUENCE si_riset.researchid
    START WITH 1004
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 3000
    CACHE 1;


ALTER TABLE si_riset.researchid OWNER TO postgres;

--
-- Name: researchid; Type: SEQUENCE OWNED BY; Schema: si_riset; Owner: postgres
--

ALTER SEQUENCE si_riset.researchid OWNED BY si_riset.research.research_id;


--
-- Name: scientific_paper; Type: TABLE; Schema: si_riset; Owner: postgres
--

CREATE TABLE si_riset.scientific_paper (
    paper_id integer NOT NULL,
    paper_title character varying(50) NOT NULL,
    paper_type character varying(20) NOT NULL,
    student_npm character varying(20) NOT NULL,
    submission_date date NOT NULL
);


ALTER TABLE si_riset.scientific_paper OWNER TO postgres;

--
-- Name: seminar_reboan; Type: TABLE; Schema: si_riset; Owner: postgres
--

CREATE TABLE si_riset.seminar_reboan (
    seminar_id integer NOT NULL,
    topic character varying(50) NOT NULL,
    speaker character varying(50) NOT NULL,
    arranged_by integer NOT NULL
);


ALTER TABLE si_riset.seminar_reboan OWNER TO postgres;

--
-- Name: student; Type: TABLE; Schema: si_riset; Owner: postgres
--

CREATE TABLE si_riset.student (
    npm character varying(20) NOT NULL,
    program_id integer NOT NULL,
    sso_username character varying(20) NOT NULL
);


ALTER TABLE si_riset.student OWNER TO postgres;

--
-- Name: study_program; Type: TABLE; Schema: si_riset; Owner: postgres
--

CREATE TABLE si_riset.study_program (
    program_id integer NOT NULL,
    degree character varying(20) NOT NULL,
    program_name character varying(20) NOT NULL
);


ALTER TABLE si_riset.study_program OWNER TO postgres;

--
-- Name: supervisor; Type: TABLE; Schema: si_riset; Owner: postgres
--

CREATE TABLE si_riset.supervisor (
    supervisor_id integer NOT NULL,
    sso_username character varying(20) NOT NULL
);


ALTER TABLE si_riset.supervisor OWNER TO postgres;

--
-- Name: user_; Type: TABLE; Schema: si_riset; Owner: postgres
--

CREATE TABLE si_riset.user_ (
    sso_username character varying(20) NOT NULL,
    sso_password character varying(20) NOT NULL,
    name character varying(50) NOT NULL,
    email character varying(50) NOT NULL,
    phone_no character varying(15) NOT NULL
);


ALTER TABLE si_riset.user_ OWNER TO postgres;

--
-- Name: auth_group id; Type: DEFAULT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.auth_group ALTER COLUMN id SET DEFAULT nextval('si_riset.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('si_riset.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.auth_permission ALTER COLUMN id SET DEFAULT nextval('si_riset.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.auth_user ALTER COLUMN id SET DEFAULT nextval('si_riset.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('si_riset.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('si_riset.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.django_admin_log ALTER COLUMN id SET DEFAULT nextval('si_riset.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.django_content_type ALTER COLUMN id SET DEFAULT nextval('si_riset.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.django_migrations ALTER COLUMN id SET DEFAULT nextval('si_riset.django_migrations_id_seq'::regclass);


--
-- Data for Name: academic_administrator; Type: TABLE DATA; Schema: si_riset; Owner: postgres
--

COPY si_riset.academic_administrator (admin_id, sso_username) FROM stdin;
1	scasse50
2	jminithorpe51
3	ggrafton52
4	agiblin53
5	mjowsey54
\.


--
-- Data for Name: approved_research; Type: TABLE DATA; Schema: si_riset; Owner: postgres
--

COPY si_riset.approved_research (approval_id, research_id, student_npm, supervisor_id, head_program_id) FROM stdin;
1	1	9194606692	1	1
2	2	8675962592	2	1
3	3	6210384706	3	1
4	4	4877138528	4	1
5	5	1960834487	5	1
6	6	7858151061	6	1
7	7	70481423	7	1
8	8	6599326099	8	1
9	9	901571814	9	1
10	10	1758845546	10	1
11	11	4643814152	11	1
12	12	1760274747	12	1
13	13	1002388228	13	1
14	14	2028029471	14	1
15	15	5408293149	15	1
16	16	3866704631	16	1
17	17	1038721091	17	1
18	18	6192792933	18	1
19	19	430838174	19	1
20	20	5197895152	20	1
21	21	578710455	21	1
22	22	4844974033	22	1
23	23	2367104921	23	1
24	24	6251398590	24	1
25	25	8211950790	25	1
26	26	6231214346	26	1
27	27	4151979271	27	1
28	28	6767661929	28	1
29	29	6556037834	29	1
30	30	6157145056	30	1
31	31	2114666921	1	1
32	32	9582801875	2	1
33	33	3734505739	3	1
34	34	9274950152	4	1
35	35	1624201032	5	1
36	36	7297500313	6	1
37	37	7998682523	7	1
38	38	6985417213	8	1
39	39	2555254757	9	1
40	40	9624425167	10	1
41	41	9262707525	11	1
42	42	9056387758	12	1
43	43	8572631550	13	1
44	44	2209096952	14	1
45	45	3868955887	15	1
46	46	1752389247	16	1
47	47	7924903488	17	1
48	48	4190014265	18	1
49	49	2017630632	19	1
50	50	6468380790	20	1
52	1002	6210384706	1	1
53	1000	9194606692	1	1
54	1004	9194606692	1	\N
\.


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: si_riset; Owner: siriset
--

COPY si_riset.auth_group (id, name) FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: si_riset; Owner: siriset
--

COPY si_riset.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: si_riset; Owner: siriset
--

COPY si_riset.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add permission	2	add_permission
5	Can change permission	2	change_permission
6	Can delete permission	2	delete_permission
7	Can add group	3	add_group
8	Can change group	3	change_group
9	Can delete group	3	delete_group
10	Can add user	4	add_user
11	Can change user	4	change_user
12	Can delete user	4	delete_user
13	Can add content type	5	add_contenttype
14	Can change content type	5	change_contenttype
15	Can delete content type	5	delete_contenttype
16	Can add session	6	add_session
17	Can change session	6	change_session
18	Can delete session	6	delete_session
19	Can add faculty member	7	add_facultymember
20	Can change faculty member	7	change_facultymember
21	Can delete faculty member	7	delete_facultymember
22	Can add head study program	8	add_headstudyprogram
23	Can change head study program	8	change_headstudyprogram
24	Can delete head study program	8	delete_headstudyprogram
25	Can add student	9	add_student
26	Can change student	9	change_student
27	Can delete student	9	delete_student
28	Can add study program	10	add_studyprogram
29	Can change study program	10	change_studyprogram
30	Can delete study program	10	delete_studyprogram
31	Can add supervisor	11	add_supervisor
32	Can change supervisor	11	change_supervisor
33	Can delete supervisor	11	delete_supervisor
34	Can add user	12	add_user
35	Can change user	12	change_user
36	Can delete user	12	delete_user
37	Can add research	13	add_research
38	Can change research	13	change_research
39	Can delete research	13	delete_research
40	Can add research type	14	add_researchtype
41	Can change research type	14	change_researchtype
42	Can delete research type	14	delete_researchtype
43	Can add approved research	15	add_approvedresearch
44	Can change approved research	15	change_approvedresearch
45	Can delete approved research	15	delete_approvedresearch
46	Can add research	16	add_research
47	Can change research	16	change_research
48	Can delete research	16	delete_research
49	Can add research type	17	add_researchtype
50	Can change research type	17	change_researchtype
51	Can delete research type	17	delete_researchtype
52	Can add approved research	18	add_approvedresearch
53	Can change approved research	18	change_approvedresearch
54	Can delete approved research	18	delete_approvedresearch
55	Can add academic administrator	19	add_academicadministrator
56	Can change academic administrator	19	change_academicadministrator
57	Can delete academic administrator	19	delete_academicadministrator
58	Can add research manager	20	add_researchmanager
59	Can change research manager	20	change_researchmanager
60	Can delete research manager	20	delete_researchmanager
61	Can add logbook	21	add_logbook
62	Can change logbook	21	change_logbook
63	Can delete logbook	21	delete_logbook
64	Can add defense	22	add_defense
65	Can change defense	22	change_defense
66	Can delete defense	22	delete_defense
67	Can add defense examiner	23	add_defenseexaminer
68	Can change defense examiner	23	change_defenseexaminer
69	Can delete defense examiner	23	delete_defenseexaminer
70	Can add scientific paper	24	add_scientificpaper
71	Can change scientific paper	24	change_scientificpaper
72	Can delete scientific paper	24	delete_scientificpaper
73	Can add seminar reboan	25	add_seminarreboan
74	Can change seminar reboan	25	change_seminarreboan
75	Can delete seminar reboan	25	delete_seminarreboan
76	Can add defense examiner	26	add_defenseexaminer
77	Can change defense examiner	26	change_defenseexaminer
78	Can delete defense examiner	26	delete_defenseexaminer
79	Can add logbook	27	add_logbook
80	Can change logbook	27	change_logbook
81	Can delete logbook	27	delete_logbook
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: si_riset; Owner: siriset
--

COPY si_riset.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: si_riset; Owner: siriset
--

COPY si_riset.auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: si_riset; Owner: siriset
--

COPY si_riset.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Data for Name: defense; Type: TABLE DATA; Schema: si_riset; Owner: postgres
--

COPY si_riset.defense (defense_id, research_id, student_npm, room, datetime, schedule_by) FROM stdin;
1	10	1758845546	1077	2017-08-02	3
2	11	4643814152	1073	2017-08-13	3
3	12	1760274747	1027	2017-06-30	3
4	13	1002388228	1095	2017-05-08	5
5	14	2028029471	1096	2017-06-28	2
6	15	5408293149	1095	2018-01-07	2
7	16	3866704631	1015	2017-09-18	3
8	17	1038721091	1024	2017-08-14	1
9	18	6192792933	1076	2017-08-16	3
10	19	430838174	1049	2017-08-27	5
11	20	5197895152	1035	2018-01-28	3
12	21	578710455	1099	2017-06-02	3
13	22	4844974033	1046	2018-03-26	4
14	23	2367104921	1034	2018-01-23	2
15	24	6251398590	1007	2018-03-05	4
16	25	8211950790	1007	2017-07-16	1
17	26	6231214346	1099	2017-07-01	4
18	27	4151979271	1006	2018-02-11	1
19	28	6767661929	1083	2018-04-22	3
20	29	6556037834	1023	2017-12-17	1
22	1	9194606692	\N	\N	\N
23	1	9194606692	\N	\N	\N
\.


--
-- Data for Name: defense_examiner; Type: TABLE DATA; Schema: si_riset; Owner: postgres
--

COPY si_riset.defense_examiner (defense_id, examiner_id) FROM stdin;
1	6
2	14
3	11
4	14
5	3
6	2
7	6
8	14
9	12
10	20
11	6
12	16
13	3
14	7
15	11
16	8
17	10
18	2
19	9
20	20
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: si_riset; Owner: siriset
--

COPY si_riset.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: si_riset; Owner: siriset
--

COPY si_riset.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	siriset_login	facultymember
8	siriset_login	headstudyprogram
9	siriset_login	student
10	siriset_login	studyprogram
11	siriset_login	supervisor
12	siriset_login	user
13	siriset_apply_research	research
14	siriset_apply_research	researchtype
15	siriset_approve	approvedresearch
16	siriset_research_apply	research
17	siriset_research_apply	researchtype
18	siriset_research_approve	approvedresearch
19	siriset_login	academicadministrator
20	siriset_login	researchmanager
21	siriset_logbook_add	logbook
22	siriset_defense_propose	defense
23	siriset_defense_schedule	defenseexaminer
24	siriset_scipaper_submit	scientificpaper
25	siriset_seminar_insert	seminarreboan
26	siriset_login	defenseexaminer
27	siriset_logbook	logbook
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: si_riset; Owner: siriset
--

COPY si_riset.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2018-05-24 14:54:20.497431+07
2	auth	0001_initial	2018-05-24 14:54:20.594634+07
3	admin	0001_initial	2018-05-24 14:54:20.616993+07
4	admin	0002_logentry_remove_auto_add	2018-05-24 14:54:20.64405+07
5	contenttypes	0002_remove_content_type_name	2018-05-24 14:54:20.664908+07
6	auth	0002_alter_permission_name_max_length	2018-05-24 14:54:20.673027+07
7	auth	0003_alter_user_email_max_length	2018-05-24 14:54:20.685294+07
8	auth	0004_alter_user_username_opts	2018-05-24 14:54:20.69784+07
9	auth	0005_alter_user_last_login_null	2018-05-24 14:54:20.708307+07
10	auth	0006_require_contenttypes_0002	2018-05-24 14:54:20.710095+07
11	auth	0007_alter_validators_add_error_messages	2018-05-24 14:54:20.721044+07
12	auth	0008_alter_user_username_max_length	2018-05-24 14:54:20.734571+07
13	auth	0009_alter_user_last_name_max_length	2018-05-24 14:54:20.746447+07
14	sessions	0001_initial	2018-05-24 14:54:20.755705+07
15	siriset_apply_research	0001_initial	2018-05-25 16:46:52.788668+07
16	siriset_approve	0001_initial	2018-05-25 16:46:52.849936+07
17	siriset_login	0001_initial	2018-05-25 16:46:52.877468+07
18	siriset_research_apply	0001_initial	2018-05-26 07:10:27.687645+07
19	siriset_research_approve	0001_initial	2018-05-26 07:10:27.74891+07
20	siriset_defense_propose	0001_initial	2018-05-26 09:05:44.110509+07
21	siriset_defense_schedule	0001_initial	2018-05-26 09:05:44.124172+07
22	siriset_logbook_add	0001_initial	2018-05-26 09:05:44.129154+07
23	siriset_login	0002_academicadministrator_researchmanager	2018-05-26 09:05:44.137774+07
24	siriset_scipaper_submit	0001_initial	2018-05-26 09:05:44.143428+07
25	siriset_seminar_insert	0001_initial	2018-05-26 09:05:44.147596+07
26	siriset_login	0003_defenseexaminer	2018-06-03 13:06:47.029118+07
27	siriset_logbook	0001_initial	2018-06-03 13:22:01.061145+07
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: si_riset; Owner: siriset
--

COPY si_riset.django_session (session_key, session_data, expire_date) FROM stdin;
p4hl6udaq6o9eggzswdow1tha1fkv5a9	Njk5MTI3ZDIzOGU1MDg3MzNmMzY3ZGU5OGEwOGUzYzA2OGRmZjE3NDp7InJvbGUiOiJzdHVkZW50IiwiaWRlbnRpdHlfY29kZSI6MSwidXNlcl9sb2dpbiI6ImZtY2Nhcm5leTQ2In0=	2018-06-18 08:38:33.007932+07
jv0ef4bfmxawfdm5998gsku89iljk6xq	NjQ5NDg4N2M3MWVjNTZhY2U3MzMzMzNhZWFjOTA1MjNiMWFmNDQ0ODp7InJvbGUiOiJzdHVkZW50IiwiaWRlbnRpdHlfY29kZSI6IjkxOTQ2MDY2OTIiLCJwcm9ncmFtX2lkIjo0LCJ1c2VyX2xvZ2luIjoicmFzcHkwIn0=	2018-06-18 08:39:23.271754+07
mrlyr3ofvdfxqlmfq99a9axcr59guxu7	YWE2MzI1NTJiYWUyYTMyNWUyNDM0YzIwNGJiM2ZjNWRiM2U0ZmMxZTp7ImtvZGVfaWRlbnRpdGFzIjoiOTE5NDYwNjY5MiIsInByb2dyYW1faWQiOjQsInVzZXJfbG9naW4iOiJyYXNweTAiLCJyb2xlIjoic3R1ZGVudCIsInJlc2VhcmNoX2lkIjoiMTAwMiJ9	2018-06-18 08:45:02.624974+07
xgz39dm9lnb2w7s7cluzrxptrsslhm7z	NjQ5NDg4N2M3MWVjNTZhY2U3MzMzMzNhZWFjOTA1MjNiMWFmNDQ0ODp7InJvbGUiOiJzdHVkZW50IiwiaWRlbnRpdHlfY29kZSI6IjkxOTQ2MDY2OTIiLCJwcm9ncmFtX2lkIjo0LCJ1c2VyX2xvZ2luIjoicmFzcHkwIn0=	2018-06-18 15:32:17.225945+07
yvhsd4jirui34i2h6o4ytfgesl2r0j8i	NjQ5NDg4N2M3MWVjNTZhY2U3MzMzMzNhZWFjOTA1MjNiMWFmNDQ0ODp7InJvbGUiOiJzdHVkZW50IiwiaWRlbnRpdHlfY29kZSI6IjkxOTQ2MDY2OTIiLCJwcm9ncmFtX2lkIjo0LCJ1c2VyX2xvZ2luIjoicmFzcHkwIn0=	2018-06-18 08:50:47.578254+07
n50k0mk2c4bf6d0ddqocpw0ubczpoqtp	MTI5YjE5NGRhYjQ1Zjg5ZGE2YzM1ZjNjN2ZhZGIxOTAxZjQwYTc3Yjp7InJvbGUiOiJzdHVkZW50IiwidXNlcl9sb2dpbiI6InJhc3B5MCIsInJlc2VhcmNoX2lkIjoiMSJ9	2018-06-18 07:16:54.726536+07
\.


--
-- Data for Name: faculty_member; Type: TABLE DATA; Schema: si_riset; Owner: postgres
--

COPY si_riset.faculty_member (faculty_member_id, education_level, sso_username) FROM stdin;
1	S2	dlannin2s
2	S2	nubee2t
3	S2	lmarconi2u
4	S2	fyancey2v
5	S2	esidnell2w
6	S2	ustollberger2x
7	S3	mmilnes2y
8	S3	nvan2z
9	S2	dmelmoth30
10	S2	morritt31
11	S3	jstubbert32
12	S3	rberkelay33
13	S3	jsanbrooke34
14	S3	blangdridge35
15	S3	wchessman36
16	S2	mmcgougan37
17	S3	nchadney38
18	S3	rmarkwelley39
19	S3	jplewman3a
20	S3	tmapstone3b
21	S2	kbrik3c
22	S2	dgredden3d
23	S2	kwegner3e
24	S2	rjikovsky3f
25	S3	lshavel3g
26	S2	bbatte3h
27	S3	gathersmith3i
28	S2	ematyugin3j
29	S3	pdrife3k
30	S3	nyoselevitch3l
31	S3	uolczyk3m
32	S3	mpopplestone3n
33	S3	aludlom3o
34	S3	mhelian3p
35	S2	dworpole3q
36	S2	tblasik3r
37	S2	lmurton3s
38	S2	mpendleberry3t
39	S3	sslite3u
40	S3	rraun3v
41	S3	hwayte3w
42	S2	kharpham3x
43	S2	thalliwell3y
44	S2	hperschke3z
45	S2	gwinterbourne40
46	S2	greace41
47	S2	lwhittlesea42
48	S2	avanini43
49	S2	bofarrell44
50	S3	gcurless45
\.


--
-- Data for Name: head_study_program; Type: TABLE DATA; Schema: si_riset; Owner: postgres
--

COPY si_riset.head_study_program (head_id, program_id, faculty_member_id, sso_username, status) FROM stdin;
1	1	1	dlannin2s	active
\.


--
-- Data for Name: logbook; Type: TABLE DATA; Schema: si_riset; Owner: postgres
--

COPY si_riset.logbook (logbook_id, datetime, location, summary, plan, research_id, supervisor_id, status) FROM stdin;
2	2017-09-23	Lailunggi	blandit non interdum in ante vestibulum ante ipsum primis in faucibus	a suscipit nulla elit ac nulla sed vel enim sit amet nunc viverra dapibus nulla suscipit ligula in	42	10	approved
3	2018-04-17	Guamal	scelerisque mauris sit amet eros suspendisse accumsan tortor quis turpis sed ante	dui luctus rutrum nulla tellus in sagittis dui vel nisl duis ac	39	29	not approved
4	2018-02-28	Bergen	fusce lacus purus aliquet at feugiat non pretium quis lectus suspendisse potenti in eleifend quam a odio in hac habitasse	in hac habitasse platea dictumst etiam faucibus cursus urna ut tellus nulla ut erat id mauris vulputate elementum	15	4	not approved
5	2017-05-10	Ayn an Nasr	quis odio consequat varius integer ac leo pellentesque ultrices mattis odio donec vitae nisi	magnis dis parturient montes nascetur ridiculus mus etiam vel augue	42	10	approved
6	2017-07-27	Cerro Blanco	in consequat ut nulla sed accumsan felis ut at dolor quis odio	et magnis dis parturient montes nascetur ridiculus mus etiam vel	45	20	not approved
7	2017-11-13	Qingminghe	suscipit a feugiat et eros vestibulum ac est lacinia nisi venenatis tristique fusce congue diam id ornare imperdiet sapien	ligula nec sem duis aliquam convallis nunc proin at turpis a pede posuere	39	2	approved
8	2017-08-23	Kingersheim	et eros vestibulum ac est lacinia nisi venenatis tristique fusce congue diam id ornare imperdiet sapien	molestie hendrerit at vulputate vitae nisl aenean lectus pellentesque eget nunc donec quis orci eget orci vehicula	17	28	approved
9	2017-08-13	Fukuoka-shi	proin leo odio porttitor id consequat in consequat ut nulla sed accumsan felis ut at	etiam pretium iaculis justo in hac habitasse platea dictumst etiam faucibus cursus urna ut tellus	5	25	approved
10	2017-08-28	Changmao	porttitor lacus at turpis donec posuere metus vitae ipsum aliquam non mauris	volutpat convallis morbi odio odio elementum eu interdum eu tincidunt in leo maecenas pulvinar lobortis	15	12	approved
11	2017-05-17	Taznakht	sapien sapien non mi integer ac neque duis bibendum morbi non quam nec dui luctus	sit amet diam in magna bibendum imperdiet nullam orci pede venenatis non sodales sed tincidunt eu felis fusce posuere	43	21	approved
12	2017-11-29	Dagui	etiam justo etiam pretium iaculis justo in hac habitasse platea dictumst etiam faucibus cursus urna ut tellus nulla ut erat	non mi integer ac neque duis bibendum morbi non quam nec dui luctus rutrum nulla tellus in sagittis dui vel	29	18	not approved
13	2018-01-01	Futuroscope	massa id lobortis convallis tortor risus dapibus augue vel accumsan	consectetuer adipiscing elit proin interdum mauris non ligula pellentesque ultrices phasellus id sapien in sapien iaculis congue vivamus metus arcu	41	8	approved
14	2018-02-12	Pontal	ultrices vel augue vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia	quis turpis sed ante vivamus tortor duis mattis egestas metus aenean fermentum donec ut mauris eget massa tempor	9	10	not approved
15	2018-01-17	Hwado	nulla ut erat id mauris vulputate elementum nullam varius nulla facilisi cras non velit nec nisi vulputate nonummy maecenas	lacinia nisi venenatis tristique fusce congue diam id ornare imperdiet sapien	31	3	approved
16	2018-02-08	Daxin	interdum mauris ullamcorper purus sit amet nulla quisque arcu libero	augue vestibulum rutrum rutrum neque aenean auctor gravida sem praesent id massa id nisl venenatis lacinia	8	15	approved
17	2017-08-06	Ceres	lacinia eget tincidunt eget tempus vel pede morbi porttitor lorem id ligula suspendisse	cras pellentesque volutpat dui maecenas tristique est et tempus semper est quam pharetra magna ac consequat	43	9	not approved
18	2017-06-25	Pagatin	eget orci vehicula condimentum curabitur in libero ut massa volutpat convallis morbi odio odio	proin at turpis a pede posuere nonummy integer non velit donec diam neque vestibulum eget vulputate	47	17	approved
19	2018-03-28	Engenho	dignissim vestibulum vestibulum ante ipsum primis in faucibus orci luctus et	libero nam dui proin leo odio porttitor id consequat in consequat ut nulla sed	2	27	not approved
20	2018-01-25	Sukarama	dui luctus rutrum nulla tellus in sagittis dui vel nisl duis ac nibh fusce lacus	rhoncus mauris enim leo rhoncus sed vestibulum sit amet cursus id turpis integer aliquet massa id	38	22	not approved
21	2017-11-06	Puksoozero	nibh in lectus pellentesque at nulla suspendisse potenti cras in purus eu magna vulputate luctus cum sociis natoque penatibus et	tincidunt eu felis fusce posuere felis sed lacus morbi sem mauris laoreet ut rhoncus	7	22	not approved
22	2017-06-19	Vancouver	leo rhoncus sed vestibulum sit amet cursus id turpis integer aliquet	metus vitae ipsum aliquam non mauris morbi non lectus aliquam sit amet diam in magna bibendum imperdiet nullam	12	4	approved
23	2018-01-09	Boulogne-Billancourt	integer non velit donec diam neque vestibulum eget vulputate ut ultrices vel	ipsum aliquam non mauris morbi non lectus aliquam sit amet diam in magna bibendum imperdiet nullam orci	12	4	approved
24	2018-01-12	Sandw_p	at vulputate vitae nisl aenean lectus pellentesque eget nunc donec quis orci eget orci vehicula condimentum curabitur in libero ut	nulla suscipit ligula in lacus curabitur at ipsum ac tellus semper	5	25	approved
25	2017-07-04	Baltimore	id ligula suspendisse ornare consequat lectus in est risus auctor sed tristique in tempus sit amet sem	at nibh in hac habitasse platea dictumst aliquam augue quam sollicitudin vitae consectetuer eget	19	24	approved
26	2017-10-29	Varennes	sit amet consectetuer adipiscing elit proin risus praesent lectus vestibulum quam sapien varius ut blandit non interdum in ante	lectus in quam fringilla rhoncus mauris enim leo rhoncus sed vestibulum sit amet cursus id turpis integer aliquet massa	42	13	approved
27	2017-12-29	Kotli Loh_r_n	ligula pellentesque ultrices phasellus id sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at vulputate vitae	rhoncus mauris enim leo rhoncus sed vestibulum sit amet cursus id turpis integer aliquet massa	22	15	not approved
28	2017-06-25	Gavarr	nulla ut erat id mauris vulputate elementum nullam varius nulla facilisi	venenatis tristique fusce congue diam id ornare imperdiet sapien urna pretium nisl ut volutpat sapien arcu sed augue aliquam erat	25	13	approved
29	2017-12-13	Serere	purus sit amet nulla quisque arcu libero rutrum ac lobortis vel dapibus at	massa volutpat convallis morbi odio odio elementum eu interdum eu tincidunt in leo maecenas	8	27	not approved
30	2017-11-25	Wengtian	orci luctus et ultrices posuere cubilia curae duis faucibus accumsan odio curabitur convallis duis consequat dui nec nisi volutpat	pellentesque eget nunc donec quis orci eget orci vehicula condimentum curabitur in libero ut massa volutpat convallis morbi	29	28	approved
31	2017-05-10	Pereval'noe	donec odio justo sollicitudin ut suscipit a feugiat et eros vestibulum ac est lacinia nisi venenatis tristique fusce congue diam	imperdiet sapien urna pretium nisl ut volutpat sapien arcu sed augue aliquam erat volutpat in congue	44	25	not approved
32	2017-07-21	Celje	congue etiam justo etiam pretium iaculis justo in hac habitasse platea dictumst etiam faucibus cursus	sapien a libero nam dui proin leo odio porttitor id consequat in consequat ut nulla	28	27	not approved
33	2018-04-05	Leones	pellentesque ultrices phasellus id sapien in sapien iaculis congue vivamus metus	accumsan tortor quis turpis sed ante vivamus tortor duis mattis egestas metus aenean fermentum donec	5	27	not approved
34	2017-07-07	Karmaskaly	at nibh in hac habitasse platea dictumst aliquam augue quam sollicitudin vitae consectetuer eget rutrum at	ut at dolor quis odio consequat varius integer ac leo pellentesque ultrices	33	11	approved
36	2017-06-22	Susuman	quis justo maecenas rhoncus aliquam lacus morbi quis tortor id nulla ultrices aliquet maecenas leo odio condimentum id luctus nec	mi nulla ac enim in tempor turpis nec euismod scelerisque	35	10	approved
37	2018-04-18	Schenectady	sed interdum venenatis turpis enim blandit mi in porttitor pede justo eu massa donec dapibus duis at	nulla pede ullamcorper augue a suscipit nulla elit ac nulla sed	30	29	not approved
38	2017-12-18	Tors	amet nunc viverra dapibus nulla suscipit ligula in lacus curabitur at ipsum ac tellus semper interdum mauris ullamcorper purus	montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque	42	12	approved
39	2017-09-30	Dongchong	ligula in lacus curabitur at ipsum ac tellus semper interdum mauris ullamcorper purus sit amet nulla quisque arcu	lectus vestibulum quam sapien varius ut blandit non interdum in ante vestibulum ante ipsum primis in	31	12	not approved
40	2017-05-01	Charenton-le-Pont	porttitor id consequat in consequat ut nulla sed accumsan felis ut at dolor quis	turpis enim blandit mi in porttitor pede justo eu massa donec dapibus duis at	4	30	not approved
41	2017-08-02	Piru	dui vel sem sed sagittis nam congue risus semper porta volutpat quam pede	hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien	24	28	not approved
42	2017-05-13	Chagou	eu magna vulputate luctus cum sociis natoque penatibus et magnis	cras in purus eu magna vulputate luctus cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus	44	6	approved
43	2017-10-16	Burgaltay	magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum	cursus vestibulum proin eu mi nulla ac enim in tempor turpis nec euismod scelerisque quam turpis	15	15	approved
44	2018-01-03	Donja Mahala	in blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum mauris non ligula	hac habitasse platea dictumst maecenas ut massa quis augue luctus tincidunt nulla mollis molestie lorem quisque	24	19	not approved
45	2017-11-13	Baixi	at feugiat non pretium quis lectus suspendisse potenti in eleifend quam a odio in hac habitasse platea dictumst maecenas ut	rutrum neque aenean auctor gravida sem praesent id massa id nisl venenatis lacinia aenean sit amet justo morbi ut odio	40	12	approved
46	2018-04-13	Guohe	montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum sociis natoque penatibus et magnis dis parturient montes nascetur	nulla integer pede justo lacinia eget tincidunt eget tempus vel pede morbi porttitor lorem id ligula suspendisse ornare consequat	39	13	not approved
47	2017-10-15	Tyarlevo	posuere cubilia curae donec pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien	justo etiam pretium iaculis justo in hac habitasse platea dictumst	39	3	not approved
48	2017-08-11	Santai	pulvinar nulla pede ullamcorper augue a suscipit nulla elit ac nulla sed vel enim	commodo placerat praesent blandit nam nulla integer pede justo lacinia eget tincidunt eget tempus vel pede morbi porttitor	25	23	not approved
49	2017-09-20	Cergy-Pontoise	gravida nisi at nibh in hac habitasse platea dictumst aliquam augue quam sollicitudin vitae consectetuer eget	proin interdum mauris non ligula pellentesque ultrices phasellus id sapien in sapien iaculis	43	30	not approved
50	2017-08-30	Villa Constitucin	arcu sed augue aliquam erat volutpat in congue etiam justo etiam pretium iaculis justo in hac	leo odio condimentum id luctus nec molestie sed justo pellentesque viverra pede ac diam cras pellentesque volutpat dui	16	27	approved
53	2018-06-21	yes	okay	okay again	1	1	not approved
35	2018-03-14	Pentapltano	dictumst morbi vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien placerat ante nulla justo	vitae quam suspendisse potenti nullam porttitor lacus at turpis donec posuere metus vitae ipsum aliquam non mauris	1	17	approved
1	2017-06-05	Hanam	lorem vitae mattis nibh ligula nec sem duis aliquam convallis nunc	vestibulum rutrum rutrum neque aenean auctor gravida sem praesent id massa id	33	23	approved
54	2018-06-22	test	ok	okkk	1002	1	not approved
55	2018-06-07	There	Yes	There	1000	1	not approved
51	2018-06-14	home	yes sir	mamamoo	1	1	approved
52	2018-06-14	another	one	yes	1	1	approved
\.


--
-- Data for Name: research; Type: TABLE DATA; Schema: si_riset; Owner: postgres
--

COPY si_riset.research (research_id, research_type_id, topic, motivation, student_npm, supervisor_id) FROM stdin;
11	2	Arecaceae	viverra pede ac diam cras pellentesque volutpat dui maecenas tristique	4643814152	11
8	1	Bromeliaceae	erat nulla tempus vivamus in felis eu sapien cursus vestibulum proin	6599326099	1
5	2	Aristolochiaceae	convallis duis consequat dui nec nisi volutpat eleifend donec ut dolor morbi	1960834487	8
10	2	Cyperaceae	non ligula pellentesque ultrices phasellus id sapien in sapien iaculis congue vivamus metus arcu adipiscing	1758845546	8
12	3	Orchidaceae	in hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque	1760274747	8
20	2	Grammitidaceae	augue vel accumsan tellus nisi eu orci mauris lacinia sapien quis libero nullam sit	5197895152	20
1	2	Cabombaceae	vestibulum proin eu mi nulla ac enim in tempor turpis nec euismod	9194606692	1
35	2	Scrophulariaceae	in lectus pellentesque at nulla suspendisse potenti cras in purus eu magna vulputate luctus cum sociis	1624201032	8
46	1	Liliaceae	ut odio cras mi pede malesuada in imperdiet et commodo vulputate justo in blandit ultrices enim	1752389247	8
21	2	Gentianaceae	neque libero convallis eget eleifend luctus ultricies eu nibh quisque id justo sit amet sapien dignissim vestibulum vestibulum ante	578710455	3
32	1	Poaceae	dolor morbi vel lectus in quam fringilla rhoncus mauris enim leo rhoncus	9582801875	3
33	1	Euphorbiaceae	pellentesque at nulla suspendisse potenti cras in purus eu magna vulputate luctus cum sociis natoque penatibus	3734505739	3
34	3	Sarraceniaceae	consequat lectus in est risus auctor sed tristique in tempus sit amet sem fusce consequat nulla nisl nunc nisl duis	9274950152	7
40	1	Polemoniaceae	tristique fusce congue diam id ornare imperdiet sapien urna pretium	9624425167	17
41	3	Salicaceae	nisl duis bibendum felis sed interdum venenatis turpis enim blandit mi in	9262707525	18
36	1	Pinaceae	consequat morbi a ipsum integer a nibh in quis justo maecenas rhoncus	7297500313	26
2	3	Pteridaceae	eget rutrum at lorem integer tincidunt ante vel ipsum praesent blandit lacinia	8675962592	2
3	2	Scrophulariaceae	elit ac nulla sed vel enim sit amet nunc viverra dapibus nulla suscipit ligula in lacus curabitur at	6210384706	3
4	3	Saxifragaceae	augue luctus tincidunt nulla mollis molestie lorem quisque ut erat curabitur gravida nisi at nibh in	4877138528	4
7	2	Brassicaceae	orci nullam molestie nibh in lectus pellentesque at nulla suspendisse	70481423	7
13	1	Asteraceae	nulla nunc purus phasellus in felis donec semper sapien a libero nam dui proin	1002388228	13
9	2	Asteraceae	aliquam erat volutpat in congue etiam justo etiam pretium iaculis justo in hac habitasse platea dictumst etiam faucibus cursus	901571814	9
15	3	Selaginellaceae	lorem integer tincidunt ante vel ipsum praesent blandit lacinia erat vestibulum sed magna	5408293149	15
19	2	Caryophyllaceae	ultrices mattis odio donec vitae nisi nam ultrices libero non mattis pulvinar	430838174	19
37	3	Convolvulaceae	duis at velit eu est congue elementum in hac habitasse platea	7998682523	22
22	1	Hymenophyllaceae	congue risus semper porta volutpat quam pede lobortis ligula sit amet eleifend pede libero quis orci nullam molestie nibh in	4844974033	22
25	3	Asteraceae	vel enim sit amet nunc viverra dapibus nulla suscipit ligula in lacus	8211950790	25
39	3	Cladoniaceae	curabitur at ipsum ac tellus semper interdum mauris ullamcorper purus sit amet	2555254757	29
42	3	Aspleniaceae	ultrices mattis odio donec vitae nisi nam ultrices libero non mattis pulvinar nulla pede	9056387758	14
43	1	Coniocybaceae	euismod scelerisque quam turpis adipiscing lorem vitae mattis nibh ligula nec sem duis aliquam convallis nunc	8572631550	15
45	1	Dryopteridaceae	non sodales sed tincidunt eu felis fusce posuere felis sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar	3868955887	17
47	1	Pottiaceae	nunc donec quis orci eget orci vehicula condimentum curabitur in libero ut massa volutpat	7924903488	19
44	3	Solanaceae	massa volutpat convallis morbi odio odio elementum eu interdum eu tincidunt in	2209096952	16
48	2	Rosaceae	rhoncus sed vestibulum sit amet cursus id turpis integer aliquet massa id	4190014265	8
49	1	Orchidaceae	erat curabitur gravida nisi at nibh in hac habitasse platea dictumst aliquam	2017630632	25
6	2	Cyperaceae	eget massa tempor convallis nulla neque libero convallis eget eleifend luctus ultricies eu nibh quisque id justo	7858151061	28
14	1	Asteraceae	massa id nisl venenatis lacinia aenean sit amet justo morbi ut odio cras mi	2028029471	28
16	2	Myrtaceae	morbi non lectus aliquam sit amet diam in magna bibendum imperdiet nullam orci pede venenatis non sodales sed tincidunt eu	3866704631	28
23	3	Asteraceae	odio cras mi pede malesuada in imperdiet et commodo vulputate justo in blandit ultrices enim lorem ipsum dolor	2367104921	28
27	2	Fabaceae	et magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum	4151979271	28
18	1	Hippocastanaceae	habitasse platea dictumst etiam faucibus cursus urna ut tellus nulla ut erat id mauris vulputate elementum nullam varius nulla	6192792933	1
24	3	Fabaceae	proin at turpis a pede posuere nonummy integer non velit donec diam neque vestibulum eget vulputate ut ultrices vel	6251398590	1
26	2	Caliciaceae	lacus curabitur at ipsum ac tellus semper interdum mauris ullamcorper purus sit amet nulla quisque arcu	6231214346	1
28	2	Fabaceae	sit amet consectetuer adipiscing elit proin risus praesent lectus vestibulum quam sapien varius ut blandit non interdum in ante	6767661929	1
29	1	Fabaceae	in lectus pellentesque at nulla suspendisse potenti cras in purus eu magna vulputate luctus cum sociis	6556037834	1
38	3	Chenopodiaceae	quam pharetra magna ac consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus	6985417213	1
50	1	Asteraceae	sit amet nunc viverra dapibus nulla suscipit ligula in lacus	6468380790	1
30	3	Betulaceae	nisi nam ultrices libero non mattis pulvinar nulla pede ullamcorper augue a suscipit nulla elit ac nulla sed	6157145056	30
31	2	Berberidaceae	vivamus tortor duis mattis egestas metus aenean fermentum donec ut mauris eget massa tempor convallis	2114666921	28
17	3	Ranunculaceae	mauris laoreet ut rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel sem	1038721091	28
1000	1	Satu	Satusatusatu	9194606692	1
1001	1	Dua	Duaduadua	8675962592	1
1002	1	Tiga	Tigatigatiga	6210384706	1
1003	1	Empat	Empatempatempat	9194606692	2
1004	1	yes	ioi	9194606692	1
1005	1	funny	nope	9194606692	1
\.


--
-- Data for Name: research_manager; Type: TABLE DATA; Schema: si_riset; Owner: postgres
--

COPY si_riset.research_manager (research_manager_id, sso_username, status) FROM stdin;
1	mmagister55	active
\.


--
-- Data for Name: research_type; Type: TABLE DATA; Schema: si_riset; Owner: postgres
--

COPY si_riset.research_type (research_type_id, research_type_name) FROM stdin;
1	Bachelor
2	Thesis
3	Independent Study
4	Doctoral
5	Specialized Study
\.


--
-- Data for Name: scientific_paper; Type: TABLE DATA; Schema: si_riset; Owner: postgres
--

COPY si_riset.scientific_paper (paper_id, paper_title, paper_type, student_npm, submission_date) FROM stdin;
1	BIVIGAM	technical report	1758845546	2017-11-25
2	Antiseptic Mouth Rinse	technical report	4643814152	2017-05-10
3	Pepto-Bismol	thesis proposal	1760274747	2017-07-21
4	Clonazepam	technical report	1002388228	2018-04-05
5	Pharbedryl	technical report	2028029471	2017-07-07
6	Multi-Symptom Cold Relief	dissertation	5408293149	2018-03-14
7	MK Alcohol Prep Pads Medium	thesis proposal	3866704631	2017-06-22
8	Opana	thesis proposal	1038721091	2018-04-18
9	Head and Shoulders	dissertation	6192792933	2017-12-18
10	DEXTROAMPHETAMINE SULFATE	thesis proposal	430838174	2017-09-30
11	allergy and congestion relief	thesis proposal	5197895152	2017-05-01
12	Butalbital, Aspirin and Caffeine Tablets	dissertation	578710455	2017-08-02
13	Regional Allergies Southern U.S.	technical report	4844974033	2017-05-13
14	Cyclobenzaprine Hydrochloride	technical report	2367104921	2017-10-16
15	Childrens Silfedrine	dissertation	6251398590	2018-01-03
16	Childrens Allergy Relief	dissertation	8211950790	2017-11-13
17	TRIPLE ANTIBIOTIC	dissertation	6231214346	2018-04-13
18	Oxygen	technical report	4151979271	2017-10-15
19	Spearmint	thesis proposal	6767661929	2017-08-11
20	GORILLA WARFARE	dissertation	6556037834	2017-09-20
\.


--
-- Data for Name: seminar_reboan; Type: TABLE DATA; Schema: si_riset; Owner: postgres
--

COPY si_riset.seminar_reboan (seminar_id, topic, speaker, arranged_by) FROM stdin;
1	Junior Prom	Ceroxylon alpinum Bonpl. ex DC.	1
2	Easy A	Navarretia rosulata Brand	1
3	Some Kind of Hero	Galeandra beyrichii Rchb. f.	1
4	Life Is What You Make It (Linha de Passe)	Lichenosticta Zopf	1
5	Hello Down There	Casuarina equisetifolia L.	1
6	Why Man Creates	Heterocladium procurrens	1
7	Ferryman, The	Perityle Benth.	1
8	Resurrect Dead: The Mystery of the Toynbee Tiles	Eriochrysis cayennensis P. Beauv.	1
9	Everything About Mustafa (Mustafa hakkinda hersey)	Collinsonia canadensis L.	1
10	300	Cuphea aspera Chapm.	1
11	Pigskin Parade	Delissea lauliiana Lammers	1
12	Tyler Perry's Madea Goes to Jail	Trifolium dalmaticum Vis.	1
13	Sand Sharks	Hieracium kalmii L	1
14	Source Code	Trichomanes javanicum Blume	1
15	Slither	Phaeographis erumpens (Nyl.)	1
16	Shrooms	Matelea cynanchoides (Engelm.) Woodson	1
17	Big Wednesday	Cyanea macrostegia Hillebre	1
18	Service (Serbis)	Lupinus ammophilus Greene	1
19	Where in the World Is Osama Bin Laden?	Salix alaxensis (Andersson) Coville	1
20	Day of Anger (I giorni dell'ira)	Glaucium flavum Crantz	1
\.


--
-- Data for Name: student; Type: TABLE DATA; Schema: si_riset; Owner: postgres
--

COPY si_riset.student (npm, program_id, sso_username) FROM stdin;
9194606692	4	raspy0
8675962592	5	mdows1
6210384706	5	edahler2
4877138528	3	esollas3
1960834487	1	cflaxman4
7858151061	3	msheepy5
70481423	3	kferriman6
6599326099	1	cboundy7
901571814	5	gspearing8
1758845546	2	ucolthard9
4643814152	2	lbleakmana
1760274747	5	enisbyb
1002388228	2	tvalenssmithc
2028029471	4	amyttond
5408293149	2	nlaurencee
3866704631	2	ckantorf
1038721091	2	mwheldong
6192792933	3	imalletth
430838174	5	abroadbenti
5197895152	5	fmaileyj
578710455	4	charmonk
4844974033	4	mlantel
2367104921	5	esquirem
6251398590	5	cdelln
8211950790	4	bcourceyo
6231214346	1	gcausonp
4151979271	1	lbruntq
6767661929	1	ctoalr
6556037834	2	dblaszczyks
6157145056	1	greicherzt
2114666921	5	kbanceu
9582801875	5	kmenaulv
3734505739	5	zstockmanw
9274950152	1	agianottix
1624201032	1	bdunhamy
7297500313	1	cscowcroftz
7998682523	3	npursey10
6985417213	4	dlaytham11
2555254757	4	silyin12
9624425167	3	etwidle13
9262707525	1	jwallman14
9056387758	1	lauston15
8572631550	2	aguyonneau16
2209096952	4	cshailer17
3868955887	2	wspurling18
1752389247	5	ghuckle19
7924903488	4	erodgier1a
4190014265	5	ahuddart1b
2017630632	5	ssearjeant1c
6468380790	1	owards1d
4000502468	3	blaneham1e
2395222240	1	ssaunt1f
497221020	3	cgligori1g
922906211	5	jingleton1h
3269259778	1	jhume1i
1736546597	2	adomerc1j
9069282178	3	cquittonden1k
5973908461	3	snertney1l
6873666599	1	vponsford1m
6079143445	1	krowlinson1n
768390311	5	gpostlethwaite1o
3543554697	1	dcrocroft1p
5864642789	4	ebrocks1q
9185346209	1	eclemencet1r
1868639096	4	scollumbell1s
6664495430	4	jcondict1t
7645905611	5	lfransinelli1u
1195479123	2	hkingswood1v
6032144887	2	rcockburn1w
9508870559	1	bbenditt1x
7569147901	1	esprey1y
773094784	1	gstelljes1z
666144400	2	brickis20
120784734	5	fcustance21
8907940584	2	mawty22
9497701322	5	crenehan23
4234598755	5	athoma24
7125660819	1	yhoyt25
9371593946	1	cparkyns26
997839546	5	abunford27
8593376495	4	sgrigoryov28
6422084463	5	hcowill29
1562990756	3	kosgordby2a
3067072510	1	jmowle2b
4243718768	2	mclemas2c
2825265861	5	lseers2d
3561032133	5	jloweth2e
61010073	4	gfairhall2f
3894509279	4	vfluger2g
3787062572	2	aivain2h
2452217212	1	djordanson2i
2433098467	2	gleyban2j
1904041213	1	ablondelle2k
6235634021	4	ocaccavari2l
6737675530	4	twareham2m
7617970889	3	bbenda2n
9803176161	2	aayscough2o
5349118331	1	hsiggery2p
3785541236	1	afroud2q
2204940941	1	hleeburn2r
\.


--
-- Data for Name: study_program; Type: TABLE DATA; Schema: si_riset; Owner: postgres
--

COPY si_riset.study_program (program_id, degree, program_name) FROM stdin;
1	doctoral	information system
2	master	computer science
3	bachelor	engineering
4	bachelor	computer science
5	doctoral	information system
\.


--
-- Data for Name: supervisor; Type: TABLE DATA; Schema: si_riset; Owner: postgres
--

COPY si_riset.supervisor (supervisor_id, sso_username) FROM stdin;
1	fmccarney46
2	bconnop47
3	mharbison48
4	osanz49
5	mbrattell4a
6	ccorcoran4b
7	hbatty4c
8	vludmann4d
9	hdeeves4e
10	nsargant4f
11	jlamonby4g
12	dspringtorpe4h
13	cboucher4i
14	oyankin4j
15	lvallentin4k
16	kposton4l
17	gredholes4m
18	mputtrell4n
19	efillary4o
20	kstickley4p
21	wmarcos4q
22	jcoupar4r
23	joreilly4s
24	asiveyer4t
25	tnation4u
26	ddemare4v
27	apattinson4w
28	mkeeler4x
29	dgraal4y
30	tcunrado4z
\.


--
-- Data for Name: user_; Type: TABLE DATA; Schema: si_riset; Owner: postgres
--

COPY si_riset.user_ (sso_username, sso_password, name, email, phone_no) FROM stdin;
raspy0	fCGSnWvo	Rhodie Aspy	raspy0@noaa.gov	5082965157
mdows1	sUGbpL1wGTK2	Marijn Dows	mdows1@shutterfly.com	9159385424
edahler2	aINusVckx	Esma Dahler	edahler2@naver.com	8543873180
esollas3	mvuyCO	Electra Sollas	esollas3@engadget.com	7804812546
cflaxman4	OfXEQgWLG	Carilyn Flaxman	cflaxman4@miitbeian.gov.cn	6143727519
msheepy5	Z0xJ6u59aLa	Mahmoud Sheepy	msheepy5@dmoz.org	3467808414
kferriman6	P6sbSxnmcUl	Klement Ferriman	kferriman6@usatoday.com	1493823166
cboundy7	FgBagu75eA	Candide Boundy	cboundy7@blogtalkradio.com	7732576375
gspearing8	Eq1mdSiH	Gaven Spearing	gspearing8@themeforest.net	8082208960
ucolthard9	qhnKz97MdgV	Upton Colthard	ucolthard9@amazon.co.uk	9103099915
lbleakmana	AfZYNuh	Lou Bleakman	lbleakmana@zimbio.com	9865732242
enisbyb	sr9lpsdzI	Elli Nisby	enisbyb@slideshare.net	6493286152
tvalenssmithc	mcsCQ3	Trisha Valens-Smith	tvalenssmithc@arstechnica.com	5545307754
amyttond	lXmOv44	Allan Mytton	amyttond@amazon.de	5009398815
nlaurencee	H1aQCkUpqt	Nevsa Laurence	nlaurencee@dion.ne.jp	3642358329
ckantorf	bnlJjT	Carry Kantor	ckantorf@forbes.com	5821077888
mwheldong	DDDsfH	Maurizio Wheldon	mwheldong@cocolog-nifty.com	3104536688
imalletth	uWGxuAZyj	Inger Mallett	imalletth@tinyurl.com	1713413016
abroadbenti	MvkPoHn	Art Broadbent	abroadbenti@elpais.com	6523892251
fmaileyj	r5Dkw5	Fonz Mailey	fmaileyj@reference.com	7573805469
charmonk	91Y2jzk	Cinderella Harmon	charmonk@slate.com	7937824510
mlantel	jbhWMlN	Mahmud Lante	mlantel@yandex.ru	8247078819
esquirem	ocRE3eZrB	Elvera Squire	esquirem@google.com.br	6203891943
cdelln	F7uMbax	Collen Dell 'Orto	cdelln@youtu.be	7772961320
bcourceyo	Nkh7zS35	Bob Courcey	bcourceyo@live.com	4195135059
gcausonp	wnJkuJ0ypkx	Genna Causon	gcausonp@altervista.org	1795097056
lbruntq	L9vyKxe98KS	Lucy Brunt	lbruntq@census.gov	1008200793
ctoalr	XW6gsZSZWFH	Chase Toal	ctoalr@paypal.com	8836210839
dblaszczyks	rtUTjoFDTD	Dugald Blaszczyk	dblaszczyks@taobao.com	3321227618
greicherzt	CNsE7RcP	Gallard Reicherz	greicherzt@instagram.com	2461368850
kbanceu	OVBb8g	Kincaid Bance	kbanceu@marketwatch.com	7363133084
kmenaulv	kEut6JwlS	Kordula Menaul	kmenaulv@live.com	5795740783
zstockmanw	1nQI6wPIfamx	Zolly Stockman	zstockmanw@hexun.com	1994895286
agianottix	crT1DWI	Aila Gianotti	agianottix@w3.org	5419255201
bdunhamy	sxXy8YkA	Berenice Dunham	bdunhamy@opensource.org	4877930287
cscowcroftz	1tvF9Bnsf1	Collie Scowcroft	cscowcroftz@kickstarter.com	1262508796
npursey10	kmx36eEoJMF	Nickolai Pursey	npursey10@liveinternet.ru	9458662299
dlaytham11	q44NB2	Delcine Laytham	dlaytham11@shop-pro.jp	7418057160
silyin12	I3a9ug2	Stanford Ilyin	silyin12@tiny.cc	2349890929
etwidle13	YUpFAoSmD	Elana Twidle	etwidle13@dyndns.org	7545658060
jwallman14	VKFRmsgWYb	Johannes Wallman	jwallman14@chron.com	6947675533
lauston15	rxjorFA6	Lani Auston	lauston15@microsoft.com	7052110659
aguyonneau16	HhViJuUn	Aila Guyonneau	aguyonneau16@answers.com	4085200156
cshailer17	SCFzuq6bb	Chloe Shailer	cshailer17@netvibes.com	7675177676
wspurling18	pyzMSh3Ue	Waylin Spurling	wspurling18@pen.io	7938437778
ghuckle19	9rGPwC	Grissel Huckle	ghuckle19@huffingtonpost.com	5441826437
erodgier1a	08od1jT	Estelle Rodgier	erodgier1a@1und1.de	8126325417
ahuddart1b	Mrp3TkB1T3Km	Ashley Huddart	ahuddart1b@baidu.com	3139313383
ssearjeant1c	0s8GwNPSj7	Sylas Searjeant	ssearjeant1c@sourceforge.net	5924302262
owards1d	u6AEFAgBG	Odette Wards	owards1d@amazonaws.com	5436247796
blaneham1e	rC6V1we	Bari Laneham	blaneham1e@walmart.com	8044111093
ssaunt1f	e1TWR4	Sherrie Saunt	ssaunt1f@ebay.com	4149771226
cgligori1g	VXzCnx	Cullin Gligori	cgligori1g@over-blog.com	8214057577
jingleton1h	bwU7hu6CtNx	Jobina Ingleton	jingleton1h@sciencedirect.com	8827772274
jhume1i	uOeegfke8gn	Johanna Hume	jhume1i@scribd.com	7302470406
adomerc1j	6ZE2kw4GD	Annadiana Domerc	adomerc1j@devhub.com	8727205738
cquittonden1k	RiCwIG	Colby Quittonden	cquittonden1k@soup.io	7945203906
snertney1l	GCVAJZT	Stanislaus Nertney	snertney1l@irs.gov	4219891430
vponsford1m	8CnO2kRvoVx	Veronique Ponsford	vponsford1m@wikipedia.org	9652042440
krowlinson1n	dp3YeCv02tLr	Kattie Rowlinson	krowlinson1n@networkadvertising.org	8193628304
gpostlethwaite1o	9QsVPI	Giraud Postlethwaite	gpostlethwaite1o@thetimes.co.uk	3991299008
dcrocroft1p	YEJi1O	Dickie Crocroft	dcrocroft1p@mysql.com	3543124099
ebrocks1q	53E2foYS	Ester Brocks	ebrocks1q@boston.com	1219832224
eclemencet1r	bmfuALSnSC	Elayne Clemencet	eclemencet1r@123-reg.co.uk	8779458373
scollumbell1s	UQz6WOfU0R	Simone Collumbell	scollumbell1s@pinterest.com	4811587639
jcondict1t	4Dc3z9U26ZS	Jodie Condict	jcondict1t@drupal.org	6405361067
lfransinelli1u	BuGq4454De81	Lammond Fransinelli	lfransinelli1u@canalblog.com	9204536793
hkingswood1v	RZ0ycpK6f	Humbert Kingswood	hkingswood1v@shinystat.com	5309599055
rcockburn1w	CXqP64h5J0gD	Roseanne Cockburn	rcockburn1w@ehow.com	5892478483
bbenditt1x	v0zTEm4RpCyE	Bruce Benditt	bbenditt1x@illinois.edu	9919341056
esprey1y	BtgywJ0bbj	Eb Sprey	esprey1y@hexun.com	7751787052
gstelljes1z	2rbEaS7I	Grier Stelljes	gstelljes1z@paypal.com	8556719985
brickis20	9ZWXGgMy	Bengt Rickis	brickis20@posterous.com	5305125514
fcustance21	cppILqf	Ferdie Custance	fcustance21@a8.net	7168202385
mawty22	LGi4vqK	Marne Awty	mawty22@devhub.com	1739509878
crenehan23	ZlPTkgxir	Cristin Renehan	crenehan23@imdb.com	1519078860
athoma24	kNZYq146aI	Aron Thoma	athoma24@pagesperso-orange.fr	6787757835
yhoyt25	cbYS4s0jf	Yance Hoyt	yhoyt25@youku.com	8524440108
cparkyns26	Bu9CJt1AYY	Chuck Parkyns	cparkyns26@canalblog.com	2256143205
abunford27	sq4qysqr4	Anallise Bunford	abunford27@earthlink.net	1485993020
sgrigoryov28	HRdqNaw8	Stacie Grigoryov	sgrigoryov28@usa.gov	9079469806
hcowill29	KCMrfr	Hasheem Cowill	hcowill29@webs.com	9582206030
kosgordby2a	EbuNDfYmo7Ca	Katharina Osgordby	kosgordby2a@biglobe.ne.jp	7519881360
jmowle2b	b0HElEMRpodK	Jerrine Mowle	jmowle2b@state.gov	2175660137
mclemas2c	djRgdOOyUky	Mayor Clemas	mclemas2c@vistaprint.com	3506659794
lseers2d	qFvTszvm	Lynsey Seers	lseers2d@hhs.gov	7886709173
jloweth2e	xDdc4eFJn2	Jacob Loweth	jloweth2e@weebly.com	6796483342
gfairhall2f	aDbaFTSiFe	Gal Fairhall	gfairhall2f@i2i.jp	7902146647
vfluger2g	apUn3F	Verile Fluger	vfluger2g@lycos.com	3287668408
aivain2h	CfbA441FChnf	Alexei Ivain	aivain2h@earthlink.net	7893892496
djordanson2i	ZrnkZWPVmSU	Dixie Jordanson	djordanson2i@xinhuanet.com	7679199142
gleyban2j	CwjL1hBLhqI	Giles Leyban	gleyban2j@parallels.com	6906660225
ablondelle2k	xhwZyXF	Andy Blondelle	ablondelle2k@sogou.com	8253010040
ocaccavari2l	dk6R3TLXpuya	Odetta Caccavari	ocaccavari2l@accuweather.com	7412162796
twareham2m	2BTEYRW4	Taryn Wareham	twareham2m@liveinternet.ru	9477468843
bbenda2n	5CLqP0ASE	Brodie Benda	bbenda2n@pen.io	3225117280
aayscough2o	dD2IXn	Allyn Ayscough	aayscough2o@google.co.uk	9065040967
hsiggery2p	72CPsdn8fQ	Harland Siggery	hsiggery2p@booking.com	9666765521
afroud2q	RoW5NG1ctj	Anthony Froud	afroud2q@xrea.com	4816524766
hleeburn2r	v78bp9fZP	Herculie Leeburn	hleeburn2r@odnoklassniki.ru	7644858147
dlannin2s	zMQmF2hB	Drucy Lannin	dlannin2s@samsung.com	2197039272
nubee2t	JsUQA8iaC	Niles Ubee	nubee2t@bloglines.com	7127953322
lmarconi2u	TmqulI2ZN8	Lauri Marconi	lmarconi2u@acquirethisname.com	9406316745
fyancey2v	nMCFHjG	Fidela Yancey	fyancey2v@home.pl	7132886878
esidnell2w	OWcuvco	Eberto Sidnell	esidnell2w@goo.ne.jp	8736594576
ustollberger2x	fyV6tbtKfF	Uriel Stollberger	ustollberger2x@cornell.edu	4046700815
mmilnes2y	nbYVMiPCpJL	Mariel Milnes	mmilnes2y@cocolog-nifty.com	4693014278
nvan2z	TG1BOuG	Nadiya Van Zon	nvan2z@bizjournals.com	9827291964
dmelmoth30	kRyQl2AmlJ	Dionysus Melmoth	dmelmoth30@ca.gov	1567931991
morritt31	PSLtydn5fz	Morgana Orritt	morritt31@dedecms.com	6172447001
jstubbert32	7XIUbS9laPL	Jannel Stubbert	jstubbert32@163.com	8221363204
rberkelay33	Fmbb7d	Roddy Berkelay	rberkelay33@imageshack.us	5134174190
jsanbrooke34	8KhrrZnsP8h6	Jannel Sanbrooke	jsanbrooke34@yelp.com	1454340781
blangdridge35	kxoWnE5m	Bartholomeo Langdridge	blangdridge35@virginia.edu	7401675617
wchessman36	fEMqWFp	Waylen Chessman	wchessman36@psu.edu	4747865778
mmcgougan37	aruG2AXfo	Moselle McGougan	mmcgougan37@fastcompany.com	3757288909
nchadney38	ZkwgKaZ	Norrie Chadney	nchadney38@sun.com	4621366527
rmarkwelley39	2strcy296N	Rossy Markwelley	rmarkwelley39@macromedia.com	3561491970
jplewman3a	eNMY0wlXpU2S	Judye Plewman	jplewman3a@harvard.edu	2797866861
tmapstone3b	4zlB6YYLc	Toiboid Mapstone	tmapstone3b@eepurl.com	7606638390
kbrik3c	iOvQWtMeASE	Karisa Brik	kbrik3c@cocolog-nifty.com	3752666276
dgredden3d	tb81fGkyN	Dorey Gredden	dgredden3d@networksolutions.com	7263413343
kwegner3e	aRTWM3ynDQSN	Kim Wegner	kwegner3e@studiopress.com	2022762359
rjikovsky3f	tKBeJXd	Raymond Jikovsky	rjikovsky3f@senate.gov	4278497903
lshavel3g	DiSBJU	Leo Shavel	lshavel3g@fotki.com	7792501227
bbatte3h	15eP0WhvBHe	Brett Batte	bbatte3h@wiley.com	5063989015
gathersmith3i	oqE07c1	Glynda Athersmith	gathersmith3i@constantcontact.com	4957868678
ematyugin3j	7aze9JRIp	Eirena Matyugin	ematyugin3j@wufoo.com	9523837970
pdrife3k	0u91FHZxr95r	Phil Drife	pdrife3k@stumbleupon.com	3613893713
nyoselevitch3l	gZo9VzmV6	Nollie Yoselevitch	nyoselevitch3l@imdb.com	5143059658
uolczyk3m	peHzQkJDo1c	Urbano Olczyk	uolczyk3m@marriott.com	4332017464
mpopplestone3n	gTBnHTWO0	Moyna Popplestone	mpopplestone3n@wikimedia.org	8534709298
aludlom3o	Wi71kdNznh	Adler Ludlom	aludlom3o@google.nl	2697782801
mhelian3p	j1nZZ6Q	Misti Helian	mhelian3p@jalbum.net	5837353136
dworpole3q	WvTZkebVJ	Donalt Worpole	dworpole3q@cbsnews.com	9703273936
tblasik3r	ZDQhMRE9E	Tate Blasik	tblasik3r@earthlink.net	4172616690
lmurton3s	tJ36ENpSNJ	Lorin Murton	lmurton3s@pcworld.com	3573711545
mpendleberry3t	ayM2qP0c	Monika Pendleberry	mpendleberry3t@ezinearticles.com	7033344156
sslite3u	6CSdEwE7	Sutherlan Slite	sslite3u@over-blog.com	9763749147
rraun3v	zeLn7j0wTfmJ	Roseanna Raun	rraun3v@cloudflare.com	4025552230
hwayte3w	V3Oi2J	Hieronymus Wayte	hwayte3w@gizmodo.com	1004091804
kharpham3x	GmYWH9UucuOa	Kippar Harpham	kharpham3x@cdbaby.com	1975688394
thalliwell3y	hkUVnwrAEyPm	Thorin Halliwell	thalliwell3y@cloudflare.com	6942532008
hperschke3z	ukM86sqf65f	Harlan Perschke	hperschke3z@utexas.edu	2582499058
gwinterbourne40	7pM7puC	Garnette Winterbourne	gwinterbourne40@china.com.cn	7003133691
greace41	QuGKNQYsFp	Gert Reace	greace41@nih.gov	2907585519
lwhittlesea42	3nR7u3ShHnp	Livvie Whittlesea	lwhittlesea42@chicagotribune.com	4511485195
avanini43	RLQvBBeUNlbl	Arlin Vanini	avanini43@barnesandnoble.com	4405926125
bofarrell44	gzxoZf1B	Bonni O'Farrell	bofarrell44@reuters.com	9659980146
gcurless45	mkDmDaQrC8sd	Graeme Curless	gcurless45@hatena.ne.jp	9048310023
fmccarney46	o0hZtUodhC	Ferdinande McCarney	fmccarney46@cbslocal.com	9919642286
bconnop47	Eyx5oP	Broderick Connop	bconnop47@barnesandnoble.com	7035964652
mharbison48	vuOZOEDkSGgk	Meredithe Harbison	mharbison48@shop-pro.jp	2878507776
osanz49	sZLXVf0tCz	Oralee Sanz	osanz49@time.com	5128361709
mbrattell4a	R2v35j	Marigold Brattell	mbrattell4a@xing.com	3515395903
ccorcoran4b	v8Nt49Isd	Cindie Corcoran	ccorcoran4b@dyndns.org	4533499995
hbatty4c	l9KFbD	Hewitt Batty	hbatty4c@nasa.gov	3224639041
vludmann4d	RIneWj	Virgilio Ludmann	vludmann4d@last.fm	5767372007
hdeeves4e	sXpZyEKMz	Hendrick Deeves	hdeeves4e@gravatar.com	5559629724
nsargant4f	XbCwudF	Neely Sargant	nsargant4f@usatoday.com	8839993821
jlamonby4g	EHEJndSGDEwN	Juanita Lamonby	jlamonby4g@google.com.au	2563244664
dspringtorpe4h	NPGp3Tc	Dorine Springtorpe	dspringtorpe4h@forbes.com	2378168667
cboucher4i	vjupArAltFwQ	Cleve Boucher	cboucher4i@businessinsider.com	5295182915
oyankin4j	MB1RKQd	Orton Yankin	oyankin4j@upenn.edu	4086906976
lvallentin4k	Fye2GqKQVwrR	Lazarus Vallentin	lvallentin4k@sohu.com	3308616342
kposton4l	kfsP0qYn	Kendal Poston	kposton4l@cbslocal.com	9062034385
gredholes4m	Dqrxm0ETFlwd	Goldy Redholes	gredholes4m@clickbank.net	4687940130
mputtrell4n	buP5wI2	Marya Puttrell	mputtrell4n@live.com	5995480589
efillary4o	xBTDzs1kFi	Elinor Fillary	efillary4o@mtv.com	8041868958
kstickley4p	60l02F	Kevan Stickley	kstickley4p@etsy.com	4717889171
wmarcos4q	4vJPpUi2I	Wallis Marcos	wmarcos4q@exblog.jp	2173710335
jcoupar4r	U0riWKB6	Janetta Coupar	jcoupar4r@chron.com	8296507627
joreilly4s	t29pkS06VK3g	Jelene O'Reilly	joreilly4s@youtube.com	1468593442
asiveyer4t	uMx50KF	Augusto Siveyer	asiveyer4t@alibaba.com	2398631444
tnation4u	FWj1P0s	Tabbatha Nation	tnation4u@feedburner.com	3047219657
ddemare4v	oZCEnq	Donn Demare	ddemare4v@opensource.org	4284782678
apattinson4w	Fjwx5ya8WbEu	Appolonia Pattinson	apattinson4w@youtube.com	3875163042
mkeeler4x	duUkx4x5lJ4w	Magdaia Keeler	mkeeler4x@ovh.net	5432025885
dgraal4y	A4fvuipNoD5	Daphne Graal	dgraal4y@mozilla.com	4221178965
tcunrado4z	YOtlWkYEN	Tonnie Cunrado	tcunrado4z@privacy.gov.au	5478634516
scasse50	jyw2hb	Sharl Casse	scasse50@diigo.com	5964865888
jminithorpe51	2FgmpPhQG	Jacintha Minithorpe	jminithorpe51@usatoday.com	3131332471
ggrafton52	fdzjVWJm	Garrek Grafton	ggrafton52@usnews.com	2101391255
agiblin53	71FA89T	Aretha Giblin	agiblin53@weebly.com	9291194245
mjowsey54	GHwSGHm	Moe Jowsey	mjowsey54@businesswire.com	9186377976
mmagister55	t4zc3I2	Melicent Magister	mmagister55@irs.gov	3479644641
ogeldeard56	9ND2n5N0rcJ	Ofelia Geldeard	ogeldeard56@nps.gov	9154652036
btombling57	8VZgjCf	Becka Tombling	btombling57@123-reg.co.uk	8125854252
felsbury58	XFcdtB	Francine Elsbury	felsbury58@phoca.cz	8163286514
bheddan59	G3cAXAl	Benji Heddan	bheddan59@tamu.edu	9385683169
ithirlwell5a	Tb95D2	Isobel Thirlwell	ithirlwell5a@eepurl.com	8956964284
aabarrow5b	0BCLWrTjQi	Arri A'Barrow	aabarrow5b@google.com.au	1466135336
astyant5c	NrLpFtvO	Anthe Styant	astyant5c@cbc.ca	1657022000
azaple5d	KhWJw3gLQLfG	Antonietta Zaple	azaple5d@mapy.cz	8072084679
kmorrow5e	318OjNv	Kaycee Morrow	kmorrow5e@ow.ly	3656788315
frilings5f	KEoj9q	Flory Rilings	frilings5f@mail.ru	9136385609
cbarratt5g	CdrQdhyMO4	Clarabelle Barratt	cbarratt5g@barnesandnoble.com	7397272113
ageator5h	2ArwEffTiEzs	Aldrich Geator	ageator5h@thetimes.co.uk	2924439662
kmarshal5i	38qfB6MvR37a	Kev Marshal	kmarshal5i@joomla.org	2947663315
kpopelay5j	fR3QT6zQPO	Karrie Popelay	kpopelay5j@vinaora.com	1596330910
\.


--
-- Name: approvalid; Type: SEQUENCE SET; Schema: si_riset; Owner: postgres
--

SELECT pg_catalog.setval('si_riset.approvalid', 54, true);


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: si_riset; Owner: siriset
--

SELECT pg_catalog.setval('si_riset.auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: si_riset; Owner: siriset
--

SELECT pg_catalog.setval('si_riset.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: si_riset; Owner: siriset
--

SELECT pg_catalog.setval('si_riset.auth_permission_id_seq', 81, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: si_riset; Owner: siriset
--

SELECT pg_catalog.setval('si_riset.auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: si_riset; Owner: siriset
--

SELECT pg_catalog.setval('si_riset.auth_user_id_seq', 1, false);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: si_riset; Owner: siriset
--

SELECT pg_catalog.setval('si_riset.auth_user_user_permissions_id_seq', 1, false);


--
-- Name: defenseid; Type: SEQUENCE SET; Schema: si_riset; Owner: postgres
--

SELECT pg_catalog.setval('si_riset.defenseid', 23, true);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: si_riset; Owner: siriset
--

SELECT pg_catalog.setval('si_riset.django_admin_log_id_seq', 1, false);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: si_riset; Owner: siriset
--

SELECT pg_catalog.setval('si_riset.django_content_type_id_seq', 27, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: si_riset; Owner: siriset
--

SELECT pg_catalog.setval('si_riset.django_migrations_id_seq', 27, true);


--
-- Name: logbookid; Type: SEQUENCE SET; Schema: si_riset; Owner: postgres
--

SELECT pg_catalog.setval('si_riset.logbookid', 55, true);


--
-- Name: researchid; Type: SEQUENCE SET; Schema: si_riset; Owner: postgres
--

SELECT pg_catalog.setval('si_riset.researchid', 1005, true);


--
-- Name: academic_administrator academic_administrator_pkey; Type: CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.academic_administrator
    ADD CONSTRAINT academic_administrator_pkey PRIMARY KEY (admin_id);


--
-- Name: approved_research approved_research_pkey; Type: CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.approved_research
    ADD CONSTRAINT approved_research_pkey PRIMARY KEY (approval_id);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: defense_examiner defense_examiner_pkey; Type: CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.defense_examiner
    ADD CONSTRAINT defense_examiner_pkey PRIMARY KEY (defense_id);


--
-- Name: defense defense_pkey; Type: CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.defense
    ADD CONSTRAINT defense_pkey PRIMARY KEY (defense_id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: faculty_member faculty_member_pkey; Type: CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.faculty_member
    ADD CONSTRAINT faculty_member_pkey PRIMARY KEY (faculty_member_id);


--
-- Name: head_study_program head_study_program_pkey; Type: CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.head_study_program
    ADD CONSTRAINT head_study_program_pkey PRIMARY KEY (head_id);


--
-- Name: logbook logbook_pkey; Type: CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.logbook
    ADD CONSTRAINT logbook_pkey PRIMARY KEY (logbook_id);


--
-- Name: research_manager research_manager_pkey; Type: CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.research_manager
    ADD CONSTRAINT research_manager_pkey PRIMARY KEY (research_manager_id);


--
-- Name: research research_pkey; Type: CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.research
    ADD CONSTRAINT research_pkey PRIMARY KEY (research_id);


--
-- Name: research_type research_type_pkey; Type: CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.research_type
    ADD CONSTRAINT research_type_pkey PRIMARY KEY (research_type_id);


--
-- Name: scientific_paper scientific_paper_pkey; Type: CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.scientific_paper
    ADD CONSTRAINT scientific_paper_pkey PRIMARY KEY (paper_id);


--
-- Name: seminar_reboan seminar_reboan_pkey; Type: CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.seminar_reboan
    ADD CONSTRAINT seminar_reboan_pkey PRIMARY KEY (seminar_id);


--
-- Name: student student_pkey; Type: CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.student
    ADD CONSTRAINT student_pkey PRIMARY KEY (npm);


--
-- Name: study_program study_program_pkey; Type: CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.study_program
    ADD CONSTRAINT study_program_pkey PRIMARY KEY (program_id);


--
-- Name: supervisor supervisor_pkey; Type: CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.supervisor
    ADD CONSTRAINT supervisor_pkey PRIMARY KEY (supervisor_id);


--
-- Name: user_ user__pkey; Type: CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.user_
    ADD CONSTRAINT user__pkey PRIMARY KEY (sso_username);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: si_riset; Owner: siriset
--

CREATE INDEX auth_group_name_a6ea08ec_like ON si_riset.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: si_riset; Owner: siriset
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON si_riset.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: si_riset; Owner: siriset
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON si_riset.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: si_riset; Owner: siriset
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON si_riset.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: si_riset; Owner: siriset
--

CREATE INDEX auth_user_groups_group_id_97559544 ON si_riset.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: si_riset; Owner: siriset
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON si_riset.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: si_riset; Owner: siriset
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON si_riset.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: si_riset; Owner: siriset
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON si_riset.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: si_riset; Owner: siriset
--

CREATE INDEX auth_user_username_6821ab7c_like ON si_riset.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: si_riset; Owner: siriset
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON si_riset.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: si_riset; Owner: siriset
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON si_riset.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: si_riset; Owner: siriset
--

CREATE INDEX django_session_expire_date_a5c62663 ON si_riset.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: si_riset; Owner: siriset
--

CREATE INDEX django_session_session_key_c0390e0f_like ON si_riset.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: academic_administrator academic_administrator_sso_username_fkey; Type: FK CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.academic_administrator
    ADD CONSTRAINT academic_administrator_sso_username_fkey FOREIGN KEY (sso_username) REFERENCES si_riset.user_(sso_username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: approved_research approved_research_head_program_id_fkey; Type: FK CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.approved_research
    ADD CONSTRAINT approved_research_head_program_id_fkey FOREIGN KEY (head_program_id) REFERENCES si_riset.head_study_program(head_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: approved_research approved_research_research_id_fkey; Type: FK CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.approved_research
    ADD CONSTRAINT approved_research_research_id_fkey FOREIGN KEY (research_id) REFERENCES si_riset.research(research_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: approved_research approved_research_student_npm_fkey; Type: FK CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.approved_research
    ADD CONSTRAINT approved_research_student_npm_fkey FOREIGN KEY (student_npm) REFERENCES si_riset.student(npm) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: approved_research approved_research_supervisor_id_fkey; Type: FK CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.approved_research
    ADD CONSTRAINT approved_research_supervisor_id_fkey FOREIGN KEY (supervisor_id) REFERENCES si_riset.supervisor(supervisor_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES si_riset.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES si_riset.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES si_riset.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES si_riset.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES si_riset.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES si_riset.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES si_riset.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: defense_examiner defense_examiner_defense_id_fkey; Type: FK CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.defense_examiner
    ADD CONSTRAINT defense_examiner_defense_id_fkey FOREIGN KEY (defense_id) REFERENCES si_riset.defense(defense_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: defense_examiner defense_examiner_examiner_id_fkey; Type: FK CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.defense_examiner
    ADD CONSTRAINT defense_examiner_examiner_id_fkey FOREIGN KEY (examiner_id) REFERENCES si_riset.faculty_member(faculty_member_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: defense defense_research_id_fkey; Type: FK CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.defense
    ADD CONSTRAINT defense_research_id_fkey FOREIGN KEY (research_id) REFERENCES si_riset.research(research_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: defense defense_schedule_by_fkey; Type: FK CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.defense
    ADD CONSTRAINT defense_schedule_by_fkey FOREIGN KEY (schedule_by) REFERENCES si_riset.academic_administrator(admin_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: defense defense_student_npm_fkey; Type: FK CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.defense
    ADD CONSTRAINT defense_student_npm_fkey FOREIGN KEY (student_npm) REFERENCES si_riset.student(npm) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES si_riset.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: si_riset; Owner: siriset
--

ALTER TABLE ONLY si_riset.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES si_riset.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: faculty_member faculty_member_sso_username_fkey; Type: FK CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.faculty_member
    ADD CONSTRAINT faculty_member_sso_username_fkey FOREIGN KEY (sso_username) REFERENCES si_riset.user_(sso_username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: head_study_program head_study_program_faculty_member_id_fkey; Type: FK CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.head_study_program
    ADD CONSTRAINT head_study_program_faculty_member_id_fkey FOREIGN KEY (faculty_member_id) REFERENCES si_riset.faculty_member(faculty_member_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: head_study_program head_study_program_program_id_fkey; Type: FK CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.head_study_program
    ADD CONSTRAINT head_study_program_program_id_fkey FOREIGN KEY (program_id) REFERENCES si_riset.study_program(program_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: head_study_program head_study_program_sso_username_fkey; Type: FK CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.head_study_program
    ADD CONSTRAINT head_study_program_sso_username_fkey FOREIGN KEY (sso_username) REFERENCES si_riset.user_(sso_username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: logbook logbook_research_id_fkey; Type: FK CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.logbook
    ADD CONSTRAINT logbook_research_id_fkey FOREIGN KEY (research_id) REFERENCES si_riset.research(research_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: logbook logbook_supervisor_id_fkey; Type: FK CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.logbook
    ADD CONSTRAINT logbook_supervisor_id_fkey FOREIGN KEY (supervisor_id) REFERENCES si_riset.supervisor(supervisor_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: research_manager research_manager_sso_username_fkey; Type: FK CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.research_manager
    ADD CONSTRAINT research_manager_sso_username_fkey FOREIGN KEY (sso_username) REFERENCES si_riset.user_(sso_username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: research research_research_type_id_fkey; Type: FK CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.research
    ADD CONSTRAINT research_research_type_id_fkey FOREIGN KEY (research_type_id) REFERENCES si_riset.research_type(research_type_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: research research_student_npm_fkey; Type: FK CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.research
    ADD CONSTRAINT research_student_npm_fkey FOREIGN KEY (student_npm) REFERENCES si_riset.student(npm) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: research research_superviosr_id_fkey; Type: FK CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.research
    ADD CONSTRAINT research_superviosr_id_fkey FOREIGN KEY (supervisor_id) REFERENCES si_riset.supervisor(supervisor_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: scientific_paper scientific_paper_student_npm_fkey; Type: FK CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.scientific_paper
    ADD CONSTRAINT scientific_paper_student_npm_fkey FOREIGN KEY (student_npm) REFERENCES si_riset.student(npm) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: seminar_reboan seminar_reboan_arranged_by_fkey; Type: FK CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.seminar_reboan
    ADD CONSTRAINT seminar_reboan_arranged_by_fkey FOREIGN KEY (arranged_by) REFERENCES si_riset.research_manager(research_manager_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: student student_program_id_fkey; Type: FK CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.student
    ADD CONSTRAINT student_program_id_fkey FOREIGN KEY (program_id) REFERENCES si_riset.study_program(program_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: student student_sso_username_fkey; Type: FK CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.student
    ADD CONSTRAINT student_sso_username_fkey FOREIGN KEY (sso_username) REFERENCES si_riset.user_(sso_username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: supervisor supervisor_sso_username_fkey; Type: FK CONSTRAINT; Schema: si_riset; Owner: postgres
--

ALTER TABLE ONLY si_riset.supervisor
    ADD CONSTRAINT supervisor_sso_username_fkey FOREIGN KEY (sso_username) REFERENCES si_riset.user_(sso_username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

