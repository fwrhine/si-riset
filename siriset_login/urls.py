from django.conf.urls import url
from .views import index, check_data, logout

app_name = 'login'

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^(?P<role>\w{0,50})/$', index, name='index'),
    url(r'^check_data/user_/$', check_data, name='check_data'),
    url(r'^logout/user_/$', logout, name='logout'),
]
