from django.db import models

# Create your models here.
class User(models.Model):
    sso_username = models.CharField(primary_key=True, max_length=20)
    sso_password = models.CharField(max_length=20)
    name = models.CharField(max_length=50)
    email = models.CharField(max_length=50)
    phone_no = models.CharField(max_length=15)

    class Meta:
        managed = False
        db_table = 'user_'

class Student(models.Model):
    npm = models.CharField(primary_key=True, max_length=20)
    program = models.ForeignKey('StudyProgram', models.DO_NOTHING)
    sso_username = models.ForeignKey('User', models.DO_NOTHING, db_column='sso_username')

    class Meta:
        managed = False
        db_table = 'student'

class StudyProgram(models.Model):
    program_id = models.IntegerField(primary_key=True)
    degree = models.CharField(max_length=20)
    program_name = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'study_program'


class Supervisor(models.Model):
    supervisor_id = models.IntegerField(primary_key=True)
    sso_username = models.ForeignKey('User', models.DO_NOTHING, db_column='sso_username')

    class Meta:
        managed = False
        db_table = 'supervisor'

class FacultyMember(models.Model):
    faculty_member_id = models.IntegerField(primary_key=True)
    education_level = models.CharField(max_length=20)
    sso_username = models.ForeignKey('User', models.DO_NOTHING, db_column='sso_username')

    class Meta:
        managed = False
        db_table = 'faculty_member'

class HeadStudyProgram(models.Model):
    head_id = models.IntegerField(primary_key=True)
    program = models.ForeignKey('StudyProgram', models.DO_NOTHING)
    faculty_member = models.ForeignKey(FacultyMember, models.DO_NOTHING)
    sso_username = models.ForeignKey('User', models.DO_NOTHING, db_column='sso_username')
    status = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'head_study_program'

class AcademicAdministrator(models.Model):
    admin_id = models.IntegerField(primary_key=True)
    sso_username = models.ForeignKey('User', models.DO_NOTHING, db_column='sso_username')

    class Meta:
        managed = False
        db_table = 'academic_administrator'

class ResearchManager(models.Model):
    research_manager_id = models.IntegerField(primary_key=True)
    sso_username = models.ForeignKey('User', models.DO_NOTHING, db_column='sso_username')
    status = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'research_manager'

class DefenseExaminer(models.Model):
    defense = models.ForeignKey('siriset_defense_propose.Defense', models.DO_NOTHING, primary_key=True)
    examiner = models.ForeignKey('FacultyMember', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'defense_examiner'
