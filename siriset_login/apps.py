from django.apps import AppConfig


class SirisetLoginConfig(AppConfig):
    name = 'siriset_login'
