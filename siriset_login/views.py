# from django.shortcuts import render
#
# response = {}
# def index(request):
#     request.session['role'] = "mahasiswa"
#     request.session['user_login'] = "raspy0"
#     return render(request, 'login.html', response)

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.db import connection

response = {}

def index(request, role):
    response['author'] = "SIRISET"
    response['is_login'] = False
    request.session['role'] = role

    if 'user_login' in request.session:
        response['is_login'] = True
        return HttpResponseRedirect(reverse('profile:index'))
    else:
        response['is_login'] = False
        html = 'login.html'
        return render(request, html, response)

def check_data(request):
    print("check data!")
    cursor = connection.cursor()
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        cursor.execute('SELECT * FROM user_ WHERE sso_username = %s and sso_password = %s;', [username, password])
        user = cursor.fetchone()
        if user is not None: # Check data in user_
            if request.session['role'] == 'student': # Check if role student
                print("a student!")
                cursor.execute('SELECT * FROM student WHERE sso_username = %s;', [username])
                student = cursor.fetchone()
                if student is not None:
                    request.session['identity_code'] = student[0]
                    request.session['program_id'] = student[1]
                    request.session['user_login'] = username
                else:
                    return HttpResponseRedirect(reverse('login:index', kwargs={'role':'student'}))

            elif request.session['role'] == 'supervisor': # Check if role supervisor
                cursor.execute('SELECT * FROM supervisor WHERE sso_username = %s;', [username])
                supervisor = cursor.fetchone()
                if supervisor is not None:
                    request.session['identity_code'] = supervisor[0]
                    request.session['user_login'] = username
                else:
                    return HttpResponseRedirect(reverse('login:index', kwargs={'role':'supervisor'}))

            elif request.session['role'] == 'research_manager': # Check if role research_manager
                cursor.execute('SELECT * FROM research_manager WHERE sso_username = %s;', [username])
                research_manager = cursor.fetchone()
                if research_manager is not None:
                    request.session['identity_code'] = research_manager[0]
                    request.session['status'] = research_manager[2]
                    request.session['user_login'] = username
                else:
                    return HttpResponseRedirect(reverse('login:index', kwargs={'role':'research_manager'}))

            elif request.session['role'] == 'academic_administrator': # Check if academic_administrator
                cursor.execute('SELECT * FROM academic_administrator WHERE sso_username = %s;', [username])
                academic_administrator = cursor.fetchone()
                if academic_administrator is not None:
                    request.session['identity_code'] = academic_administrator[0]
                    request.session['user_login'] = username
                else:
                    return HttpResponseRedirect(reverse('login:index', kwargs={'role':'academic_administrator'}))

            elif request.session['role'] == 'head_study_program': # Check if role faculty_member
                cursor.execute('SELECT * FROM faculty_member WHERE sso_username = %s;', [username])
                faculty_member = cursor.fetchone()
                if faculty_member is not None:
                    cursor.execute('SELECT * FROM head_study_program WHERE faculty_member_id = %s;', [faculty_member[0]])
                    head_study_program = cursor.fetchone()
                    if head_study_program is not None:
                        request.session['identity_code'] = head_study_program[0]
                        request.session['program_id'] = head_study_program[1]
                        request.session['faculty_member_id'] = head_study_program[2]
                        request.session['status'] = head_study_program[4]
                        request.session['user_login'] = username
                else:
                    return HttpResponseRedirect(reverse('login:index', kwargs={'role':'head_study_program'}))

        else: # redirect if not user_
            return HttpResponseRedirect(reverse('login:index', kwargs={'role':request.session['role']}))

    return HttpResponseRedirect(reverse('profile:index'))

def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('login:index', kwargs={'role':'student'}))

def authenticate(request):
	return ('user_login' in request.session.keys())
