from django.apps import AppConfig


class SirisetScipaperSubmitConfig(AppConfig):
    name = 'siriset_scipaper_submit'
