from django.shortcuts import render
from django.db import connection, transaction

response = {}
def index(request):
    cursor = connection.cursor()
    sql = 'SELECT rt.research_type_name, rt.research_type_id\
        FROM research_type rt'

    cursor.execute(sql)
    research = dictfetchall(cursor)
    response['research'] = research
    return render(request, 'scipaper_submit.html', response)

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
