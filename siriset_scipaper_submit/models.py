from django.db import models

# Create your models here.
class ScientificPaper(models.Model):
    paper_id = models.IntegerField(primary_key=True)
    paper_title = models.CharField(max_length=50)
    paper_type = models.CharField(max_length=20)
    student_npm = models.ForeignKey('siriset_login.Student', models.DO_NOTHING, db_column='student_npm')
    submission_date = models.DateField()

    class Meta:
        managed = False
        db_table = 'scientific_paper'
