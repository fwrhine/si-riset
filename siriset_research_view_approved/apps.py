from django.apps import AppConfig


class SirisetResearchViewApprovedConfig(AppConfig):
    name = 'siriset_research_view_approved'
