from django.shortcuts import render
from django.db import connection, transaction

response = {}

def index(request):
    cursor = connection.cursor()

    if request.session['role'] == "student":
        sql = 'SELECT r.research_id, rt.research_type_name, r.topic, u.name\
            FROM approved_research a, research_type rt, research r, supervisor s, user_ u, student st\
            WHERE a.research_id = r.research_id and r.research_type_id = rt.research_type_id\
            and r.supervisor_id = s.supervisor_id and s.sso_username = u.sso_username\
            and a.student_npm = st.npm and st.sso_username = %s and a.head_program_id is not null'

        cursor.execute(sql, [request.session['user_login']])
    elif request.session['role'] == "supervisor":
        sql = 'SELECT r.research_id, u.name, sp.degree, rt.research_type_name, r.topic\
            FROM approved_research a, research_type rt, research r, user_ u, student st,\
            study_program sp, supervisor s\
            WHERE a.research_id = r.research_id and a.student_npm = st.npm\
            and st.sso_username = u.sso_username and st.program_id = sp.program_id\
            and r.research_type_id = rt.research_type_id and a.supervisor_id = s.supervisor_id\
            and s.sso_username = %s and a.head_program_id is not null'

        cursor.execute(sql, [request.session['user_login']])
    elif request.session['role'] == "head_study_program":
        sql = 'SELECT r.research_id, u.name as student_name, sp.degree, rt.research_type_name, r.topic, u2.name super_name\
            FROM approved_research a, research_type rt, research r, user_ u, user_ u2,\
            student st, study_program sp, supervisor s\
            WHERE a.research_id = r.research_id and a.student_npm = st.npm\
            and st.sso_username = u.sso_username and st.program_id = sp.program_id\
            and r.research_type_id = rt.research_type_id and a.supervisor_id = s.supervisor_id\
            and s.sso_username = u2.sso_username and a.head_program_id is not null'

        cursor.execute(sql)

    print(cursor)
    approved = dictfetchall(cursor)

    response['author'] = "si-riset"
    response['approved'] = approved
    return render(request, 'research_view_approved.html', response)

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
